#include "gtest\gtest.h"
#include "engine\Card.h"

namespace bulbul
{
    TEST(CardTest, TestIsAnimalCard)
    {
        EXPECT_FALSE(IsAnimalCard(Card::JOKER));
        EXPECT_FALSE(IsAnimalCard(Card::PLUS_TWO));
        EXPECT_TRUE(IsAnimalCard(Card::RAT));
        EXPECT_TRUE(IsAnimalCard(Card::RABBIT));
        EXPECT_TRUE(IsAnimalCard(Card::SNAKE));
        EXPECT_TRUE(IsAnimalCard(Card::SHEEP));
        EXPECT_TRUE(IsAnimalCard(Card::MONKEY));
        EXPECT_TRUE(IsAnimalCard(Card::ROOSTER));
        EXPECT_TRUE(IsAnimalCard(Card::DOG));
        EXPECT_FALSE(IsAnimalCard(Card::NUMBER_OF_ALL_CARDS));
    }

    TEST(CardTest, TestIsSpecialCard)
    {
        EXPECT_TRUE(IsSpecialCard(Card::JOKER));
        EXPECT_TRUE(IsSpecialCard(Card::PLUS_TWO));
        EXPECT_FALSE(IsSpecialCard(Card::RAT));
        EXPECT_FALSE(IsSpecialCard(Card::RABBIT));
        EXPECT_FALSE(IsSpecialCard(Card::SNAKE));
        EXPECT_FALSE(IsSpecialCard(Card::SHEEP));
        EXPECT_FALSE(IsSpecialCard(Card::MONKEY));
        EXPECT_FALSE(IsSpecialCard(Card::ROOSTER));
        EXPECT_FALSE(IsSpecialCard(Card::DOG));
        EXPECT_FALSE(IsSpecialCard(Card::NUMBER_OF_ALL_CARDS));
    }
    
    TEST(CardTest, TestIsPlayableCard)
    {
        EXPECT_TRUE(IsPlayableCard(Card::JOKER));
        EXPECT_TRUE(IsPlayableCard(Card::PLUS_TWO));
        EXPECT_TRUE(IsPlayableCard(Card::RAT));
        EXPECT_TRUE(IsPlayableCard(Card::RABBIT));
        EXPECT_TRUE(IsPlayableCard(Card::SNAKE));
        EXPECT_TRUE(IsPlayableCard(Card::SHEEP));
        EXPECT_TRUE(IsPlayableCard(Card::MONKEY));
        EXPECT_TRUE(IsPlayableCard(Card::ROOSTER));
        EXPECT_TRUE(IsPlayableCard(Card::DOG));
        EXPECT_FALSE(IsPlayableCard(Card::NUMBER_OF_ALL_CARDS));
    }

	TEST(CardTest, CanIterateThroughCardsUsingForLoop)
	{
		Card card = Card::FIRST_CARD;
		unsigned int count = static_cast<unsigned int>(Card::NUMBER_OF_ALL_CARDS);
		unsigned int currentCount = 0;
		for ( ; card <= Card::LAST_CARD ; ++card)
		{
			++currentCount;
		}
		EXPECT_EQ(Card::PAST_ALL_CARDS, card);
		EXPECT_EQ(count, currentCount);
	}

	TEST(CardTest, CanIterateThroughSpecialCardsUsingForLoop)
	{
		Card card = Card::FIRST_SPECIAL_CARD;
		unsigned int count = static_cast<unsigned int>(Card::NUMBER_OF_SPECIAL_CARDS);
		unsigned int currentCount = 0;
		for ( ; card <= Card::LAST_SPECIAL_CARD ; ++card)
		{
			++currentCount;
		}
		EXPECT_EQ(Card::PAST_SPECIAL_CARDS, card);
		EXPECT_EQ(count, currentCount);
	}

	TEST(CardTest, CanIterateThroughAnimalCardsUsingForLoop)
	{
		Card card = Card::FIRST_ANIMAL_CARD;
		unsigned int count = static_cast<unsigned int>(Card::NUMBER_OF_ANIMAL_CARDS);
		unsigned int currentCount = 0;
		for ( ; card <= Card::LAST_ANIMAL_CARD ; ++card)
		{
			++currentCount;
		}
		EXPECT_EQ(Card::PAST_ANIMAL_CARDS, card);
		EXPECT_EQ(count, currentCount);
	}

    TEST(CardTest, CannotIteratePastLastCard)
    {
        Card card = Card::PAST_ALL_CARDS;
        EXPECT_THROW(++card, IncrementedPastLastCardError);
    }
}
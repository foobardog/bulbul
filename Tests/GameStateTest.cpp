#include <algorithm>
#include <numeric>
#include <random>
#include <set>
#include <utility>
#include "gtest\gtest.h"
#include "engine\Card.h"
#include "engine\GameState.h"
#include "engine\Pile.h"

using std::accumulate;
using std::count_if;
using std::default_random_engine;
using std::equal;
using std::pair;
using std::set;

namespace bulbul
{
    TEST(GameStateTest, CanCreateTwoPlayerGame)
    {
        default_random_engine engine;
        unsigned int numberOfPlayers = 2;
        GameState<default_random_engine> gameState(numberOfPlayers, engine);
        EXPECT_EQ(numberOfPlayers, gameState.GetNumberOfPlayers());
        EXPECT_EQ(0, gameState.GetCurrentPlayer());
        EXPECT_FALSE(gameState.IsFinalRound());
        EXPECT_FALSE(gameState.IsGameOver());
        EXPECT_FALSE(gameState.IsCardDrawn());
        EXPECT_EQ(gameState.NO_CARD_DRAWN, gameState.GetDrawnCard());

        // Player cards (checked first because the deck checks use it)
        set<Card> dealtPlayerCards;
        for (unsigned int currentPlayer = 0; currentPlayer < numberOfPlayers; ++currentPlayer)
        {
            const Pile& currentPlayerCards = gameState.GetPlayerCards(currentPlayer);
            EXPECT_EQ(gameState.TWO_PLAYER_STARTING_CARD_COUNT, currentPlayerCards.GetNumberOfCards());
            for (Card cardType : currentPlayerCards.GetCardTypes())
            {
                EXPECT_TRUE(IsAnimalCard(cardType));
                EXPECT_EQ(1, currentPlayerCards.GetCardCount(cardType));
                dealtPlayerCards.insert(cardType);
            }
        }
        EXPECT_EQ(4, dealtPlayerCards.size());
        
        // Deck
        const vector<Card>& deck = gameState.GetDeck();
        EXPECT_EQ(54, gameState.GetDeckSize());
        EXPECT_EQ(41, count_if(deck.begin(), deck.end(), IsAnimalCard));
        
        // Animal Cards
        set<Card> animalCardsInDeck;
        for (auto currentCard = deck.begin(); currentCard != deck.end(); ++currentCard)
        {
            if (IsAnimalCard(*currentCard))
            {
                animalCardsInDeck.insert(*currentCard);
            }
        }
        EXPECT_EQ(5, animalCardsInDeck.size());

        for (auto currentAnimalCard = animalCardsInDeck.begin(); currentAnimalCard != animalCardsInDeck.end(); ++currentAnimalCard)
        {
            unsigned int cardCount = 
                count_if(deck.begin(), deck.end(), [currentAnimalCard] (Card card) { return card == *currentAnimalCard; });
            unsigned int expectedCardCount = dealtPlayerCards.find(*currentAnimalCard) == dealtPlayerCards.end() ? 
                gameState.CARDS_PER_ANIMAL_CARD_SET : gameState.CARDS_PER_ANIMAL_CARD_SET - 1;
            EXPECT_EQ(expectedCardCount, cardCount);
        }

        // Special Cards
        EXPECT_EQ(gameState.JOKER_CARDS_TO_ADD, count_if(deck.begin(), deck.end(), [] (Card card) { return card == Card::JOKER; }));
        EXPECT_EQ(gameState.PLUS_TWO_CARDS_TO_ADD, count_if(deck.begin(), deck.end(), [] (Card card) { return card == Card::PLUS_TWO; }));

        // Piles
        EXPECT_EQ(gameState.TWO_PLAYER_STARTING_PILES, gameState.GetNumberOfPiles());
        for (unsigned int currentPile = 0; currentPile < gameState.TWO_PLAYER_STARTING_PILES; ++currentPile)
        {
            const Pile& currentPileCards = gameState.GetPile(currentPile);
            EXPECT_EQ(0, currentPileCards.GetNumberOfCards());
        }
    }

    TEST(GameStateTest, CanCreateThreePlayerGame)
    {
        default_random_engine engine;
        unsigned int numberOfPlayers = 3;
        GameState<default_random_engine> gameState(numberOfPlayers, engine);
        EXPECT_EQ(numberOfPlayers, gameState.GetNumberOfPlayers());
        EXPECT_EQ(0, gameState.GetCurrentPlayer());
        EXPECT_FALSE(gameState.IsFinalRound());
        EXPECT_FALSE(gameState.IsGameOver());
        EXPECT_FALSE(gameState.IsCardDrawn());
        EXPECT_EQ(gameState.NO_CARD_DRAWN, gameState.GetDrawnCard());

        // Player cards (checked first because the deck checks use it)
        set<Card> dealtPlayerCards;
        for (unsigned int currentPlayer = 0; currentPlayer < numberOfPlayers; ++currentPlayer)
        {
            const Pile& currentPlayerCards = gameState.GetPlayerCards(currentPlayer);
            EXPECT_EQ(1, currentPlayerCards.GetNumberOfCards());
            for (Card cardType : currentPlayerCards.GetCardTypes())
            {
                EXPECT_TRUE(IsAnimalCard(cardType));
                EXPECT_EQ(1, currentPlayerCards.GetCardCount(cardType));
                dealtPlayerCards.insert(cardType);
            }
        }
        EXPECT_EQ(numberOfPlayers, dealtPlayerCards.size());
        
        // Deck
        const vector<Card>& deck = gameState.GetDeck();
        EXPECT_EQ(64, gameState.GetDeckSize());
        EXPECT_EQ(51, count_if(deck.begin(), deck.end(), IsAnimalCard));
        
        // Animal Cards
        set<Card> animalCardsInDeck;
        for (auto currentCard = deck.begin(); currentCard != deck.end(); ++currentCard)
        {
            if (IsAnimalCard(*currentCard))
            {
                animalCardsInDeck.insert(*currentCard);
            }
        }
        EXPECT_EQ(6, animalCardsInDeck.size());

        for (auto currentAnimalCard = animalCardsInDeck.begin(); currentAnimalCard != animalCardsInDeck.end(); ++currentAnimalCard)
        {
            unsigned int cardCount = 
                count_if(deck.begin(), deck.end(), [currentAnimalCard] (Card card) { return card == *currentAnimalCard; });
            unsigned int expectedCardCount = dealtPlayerCards.find(*currentAnimalCard) == dealtPlayerCards.end() ? 
                gameState.CARDS_PER_ANIMAL_CARD_SET : gameState.CARDS_PER_ANIMAL_CARD_SET - 1;
            EXPECT_EQ(expectedCardCount, cardCount);
        }

        // Special Cards
        EXPECT_EQ(gameState.JOKER_CARDS_TO_ADD, count_if(deck.begin(), deck.end(), [] (Card card) { return card == Card::JOKER; }));
        EXPECT_EQ(gameState.PLUS_TWO_CARDS_TO_ADD, count_if(deck.begin(), deck.end(), [] (Card card) { return card == Card::PLUS_TWO; }));

        // Piles
        EXPECT_EQ(numberOfPlayers, gameState.GetNumberOfPiles());
        for (unsigned int currentPile = 0; currentPile < numberOfPlayers; ++currentPile)
        {
            const Pile& currentPileCards = gameState.GetPile(currentPile);
            EXPECT_EQ(0, currentPileCards.GetNumberOfCards());
        }
    }

    TEST(GameStateTest, CanCreateFourPlayerGame)
    {
        default_random_engine engine;
        unsigned int numberOfPlayers = 4;
        GameState<default_random_engine> gameState(numberOfPlayers, engine);
        EXPECT_EQ(numberOfPlayers, gameState.GetNumberOfPlayers());
        EXPECT_EQ(0, gameState.GetCurrentPlayer());
        EXPECT_FALSE(gameState.IsFinalRound());
        EXPECT_FALSE(gameState.IsGameOver());
        EXPECT_FALSE(gameState.IsCardDrawn());
        EXPECT_EQ(gameState.NO_CARD_DRAWN, gameState.GetDrawnCard());

        // Player cards (checked first because the deck checks use it)
        set<Card> dealtPlayerCards;
        for (unsigned int currentPlayer = 0; currentPlayer < numberOfPlayers; ++currentPlayer)
        {
            const Pile& currentPlayerCards = gameState.GetPlayerCards(currentPlayer);
            EXPECT_EQ(1, currentPlayerCards.GetNumberOfCards());
            for (Card cardType : currentPlayerCards.GetCardTypes())
            {
                EXPECT_TRUE(IsAnimalCard(cardType));
                EXPECT_EQ(1, currentPlayerCards.GetCardCount(cardType));
                dealtPlayerCards.insert(cardType);
            }
        }
        EXPECT_EQ(numberOfPlayers, dealtPlayerCards.size());
        
        // Deck
        const vector<Card>& deck = gameState.GetDeck();
        EXPECT_EQ(72, gameState.GetDeckSize());
        EXPECT_EQ(59, count_if(deck.begin(), deck.end(), IsAnimalCard));
        
        // Animal Cards
        set<Card> animalCardsInDeck;
        for (auto currentCard = deck.begin(); currentCard != deck.end(); ++currentCard)
        {
            if (IsAnimalCard(*currentCard))
            {
                animalCardsInDeck.insert(*currentCard);
            }
        }
        EXPECT_EQ(7, animalCardsInDeck.size());

        for (auto currentAnimalCard = animalCardsInDeck.begin(); currentAnimalCard != animalCardsInDeck.end(); ++currentAnimalCard)
        {
            unsigned int cardCount = 
                count_if(deck.begin(), deck.end(), [currentAnimalCard] (Card card) { return card == *currentAnimalCard; });
            unsigned int expectedCardCount = dealtPlayerCards.find(*currentAnimalCard) == dealtPlayerCards.end() ? 
                gameState.CARDS_PER_ANIMAL_CARD_SET : gameState.CARDS_PER_ANIMAL_CARD_SET - 1;
            EXPECT_EQ(expectedCardCount, cardCount);
        }

        // Special Cards
        EXPECT_EQ(gameState.JOKER_CARDS_TO_ADD, count_if(deck.begin(), deck.end(), [] (Card card) { return card == Card::JOKER; }));
        EXPECT_EQ(gameState.PLUS_TWO_CARDS_TO_ADD, count_if(deck.begin(), deck.end(), [] (Card card) { return card == Card::PLUS_TWO; }));

        // Piles
        EXPECT_EQ(numberOfPlayers, gameState.GetNumberOfPiles());
        for (unsigned int currentPile = 0; currentPile < numberOfPlayers; ++currentPile)
        {
            const Pile& currentPileCards = gameState.GetPile(currentPile);
            EXPECT_EQ(0, currentPileCards.GetNumberOfCards());
        }
    }

    TEST(GameStateTest, CanCreateFivePlayerGame)
    {
        default_random_engine engine;
        unsigned int numberOfPlayers = 5;
        GameState<default_random_engine> gameState(numberOfPlayers, engine);
        EXPECT_EQ(numberOfPlayers, gameState.GetNumberOfPlayers());
        EXPECT_EQ(0, gameState.GetCurrentPlayer());
        EXPECT_FALSE(gameState.IsFinalRound());
        EXPECT_FALSE(gameState.IsGameOver());
        EXPECT_FALSE(gameState.IsCardDrawn());
        EXPECT_EQ(gameState.NO_CARD_DRAWN, gameState.GetDrawnCard());

        // Player cards (checked first because the deck checks use it)
        set<Card> dealtPlayerCards;
        for (unsigned int currentPlayer = 0; currentPlayer < numberOfPlayers; ++currentPlayer)
        {
            const Pile& currentPlayerCards = gameState.GetPlayerCards(currentPlayer);
            EXPECT_EQ(1, currentPlayerCards.GetNumberOfCards());
            for (Card cardType : currentPlayerCards.GetCardTypes())
            {
                EXPECT_TRUE(IsAnimalCard(cardType));
                EXPECT_EQ(1, currentPlayerCards.GetCardCount(cardType));
                dealtPlayerCards.insert(cardType);
            }
        }
        EXPECT_EQ(numberOfPlayers, dealtPlayerCards.size());
        
        // Deck
        const vector<Card>& deck = gameState.GetDeck();
        EXPECT_EQ(71, gameState.GetDeckSize());
        EXPECT_EQ(58, count_if(deck.begin(), deck.end(), IsAnimalCard));
        
        // Animal Cards
        set<Card> animalCardsInDeck;
        for (auto currentCard = deck.begin(); currentCard != deck.end(); ++currentCard)
        {
            if (IsAnimalCard(*currentCard))
            {
                animalCardsInDeck.insert(*currentCard);
            }
        }
        EXPECT_EQ(7, animalCardsInDeck.size());

        for (auto currentAnimalCard = animalCardsInDeck.begin(); currentAnimalCard != animalCardsInDeck.end(); ++currentAnimalCard)
        {
            unsigned int cardCount = 
                count_if(deck.begin(), deck.end(), [currentAnimalCard] (Card card) { return card == *currentAnimalCard; });
            unsigned int expectedCardCount = dealtPlayerCards.find(*currentAnimalCard) == dealtPlayerCards.end() ? 
                gameState.CARDS_PER_ANIMAL_CARD_SET : gameState.CARDS_PER_ANIMAL_CARD_SET - 1;
            EXPECT_EQ(expectedCardCount, cardCount);
        }

        // Special Cards
        EXPECT_EQ(gameState.JOKER_CARDS_TO_ADD, count_if(deck.begin(), deck.end(), [] (Card card) { return card == Card::JOKER; }));
        EXPECT_EQ(gameState.PLUS_TWO_CARDS_TO_ADD, count_if(deck.begin(), deck.end(), [] (Card card) { return card == Card::PLUS_TWO; }));

        // Piles
        EXPECT_EQ(numberOfPlayers, gameState.GetNumberOfPiles());
        for (unsigned int currentPile = 0; currentPile < numberOfPlayers; ++currentPile)
        {
            const Pile& currentPileCards = gameState.GetPile(currentPile);
            EXPECT_EQ(0, currentPileCards.GetNumberOfCards());
        }
    }

    TEST(GameStateTest, CardCanBeDrawn)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(3, engine);
        unsigned int originalSize = gameState.GetDeck().size();
        Card drawnCard = gameState.DrawCard();
        const vector<Card>& deck = gameState.GetDeck();
        EXPECT_EQ(Card::ROOSTER, drawnCard);
        EXPECT_EQ(drawnCard, gameState.GetDrawnCard());
        EXPECT_EQ(originalSize - 1, deck.size());
        EXPECT_EQ(0, gameState.GetCurrentPlayer());
    }

    TEST(GameStateTest, CardCannotBeDrawnTwice)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(3, engine);
        Card drawnCard = gameState.DrawCard();
        EXPECT_THROW(gameState.DrawCard(), CannotDrawTwiceWithoutPlacingError);
    }

    TEST(GameStateTest, CardCanBePlaced)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(2, engine);
        const Pile& playerCards1Before = gameState.GetPlayerCards(0);
        const Pile& playerCards2Before = gameState.GetPlayerCards(1);
        Card drawnCard = gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        EXPECT_FALSE(gameState.IsCardDrawn());
        EXPECT_EQ(gameState.NO_CARD_DRAWN, gameState.GetDrawnCard());
        const Pile& pile1 = gameState.GetPile(0);
        EXPECT_EQ(1, pile1.GetCardCount(drawnCard));
        const Pile& pile2 = gameState.GetPile(1);
        EXPECT_EQ(0, pile2.GetCardCount(drawnCard));
        const Pile& pile3 = gameState.GetPile(2);
        EXPECT_EQ(0, pile3.GetCardCount(drawnCard));
        const Pile& playerCards1After = gameState.GetPlayerCards(0);
        EXPECT_EQ(playerCards1Before, playerCards1After);
        const Pile& playerCards2After = gameState.GetPlayerCards(1);
        EXPECT_EQ(playerCards2Before, playerCards2After);
        const vector<Card>& deck = gameState.GetDeck();
        EXPECT_EQ(53, deck.size());
        EXPECT_EQ(1, gameState.GetCurrentPlayer());
    }

    TEST(GameStateTest, MultiplePlacesUpdatesCurrentPlayerCorrectly)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(3, engine);
        unsigned int turn1 = gameState.GetCurrentPlayer();
        EXPECT_EQ(0, turn1);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        unsigned int turn2 = gameState.GetCurrentPlayer();
        EXPECT_EQ(1, turn2);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(1);
        unsigned int turn3 = gameState.GetCurrentPlayer();
        EXPECT_EQ(2, turn3);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(2);
        unsigned int turn4 = gameState.GetCurrentPlayer();
        EXPECT_EQ(0, turn4);
    }

    TEST(GameStateTest, MaxPlacesThreePlayerPlusGame)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(4, engine);
        gameState.DrawCard();
        for (unsigned int currPile = 0; currPile < gameState.GetNumberOfPiles(); ++currPile)
        {
            gameState.PlaceCardOnPile(currPile);
            gameState.DrawCard();
            gameState.PlaceCardOnPile(currPile);
            gameState.DrawCard();
            gameState.PlaceCardOnPile(currPile);
            gameState.DrawCard();
            ASSERT_THROW(gameState.PlaceCardOnPile(currPile), CannotPlaceOnFullPileError);
        }
    }

    TEST(GameStateTest, MaxPlacesTwoPlayerGame)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(2, engine);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        gameState.DrawCard();
        ASSERT_THROW(gameState.PlaceCardOnPile(0), CannotPlaceOnFullPileError);
        gameState.PlaceCardOnPile(1);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(1);
        gameState.DrawCard();
        ASSERT_THROW(gameState.PlaceCardOnPile(1), CannotPlaceOnFullPileError);
        gameState.PlaceCardOnPile(2);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(2);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(2);
        gameState.DrawCard();
        ASSERT_THROW(gameState.PlaceCardOnPile(2), CannotPlaceOnFullPileError);
    }

    TEST(GameStateTest, MaxPlacesTwoPlayerGameAfterTaking)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(2, engine);
        EXPECT_EQ(1, gameState.GetMaximumCardsPerPile(0));
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        gameState.TakePile(0);
        EXPECT_EQ(2, gameState.GetMaximumCardsPerPile(1));
        gameState.DrawCard();
        gameState.PlaceCardOnPile(1);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(1);
        gameState.DrawCard();
        ASSERT_THROW(gameState.PlaceCardOnPile(1), CannotPlaceOnFullPileError);
        EXPECT_EQ(3, gameState.GetMaximumCardsPerPile(2));
        gameState.PlaceCardOnPile(2);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(2);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(2);
        gameState.DrawCard();
        ASSERT_THROW(gameState.PlaceCardOnPile(2), CannotPlaceOnFullPileError);
    }

    TEST(GameStateTest, CannotPlaceWhenNoCardDrawn)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(5, engine);
        EXPECT_THROW(gameState.PlaceCardOnPile(0), CannotPlaceWhenNoCardDrawnError);
    }

    TEST(GameStateTest, CanTakeSingleCardPile)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(2, engine);
        const Pile& playerCards1Before = gameState.GetPlayerCards(0);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        gameState.TakePile(0);
        EXPECT_EQ(3, gameState.GetNumberOfPiles());
        Pile playerCards1After = gameState.GetPlayerCards(0);
        EXPECT_EQ(playerCards1Before, playerCards1After);
        Pile playerCards2After = gameState.GetPlayerCards(1);
        EXPECT_EQ(3, playerCards2After.GetNumberOfCards());
        EXPECT_EQ(1, playerCards2After.GetCardCount(Card::SNAKE));
        EXPECT_EQ(2, playerCards2After.GetCardCount(Card::SHEEP));
        const set<unsigned int>& playersThatHaveTaken = gameState.GetPlayersThatHaveTaken();
        EXPECT_TRUE(playersThatHaveTaken.find(0) == playersThatHaveTaken.end());
        EXPECT_TRUE(playersThatHaveTaken.find(1) != playersThatHaveTaken.end());
        EXPECT_EQ(0, gameState.GetCurrentPlayer());
    }

    TEST(GameStateTest, CanTakeDoubleCardPile)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(3, engine);
        const Pile& playerCards1Before = gameState.GetPlayerCards(0);
        const Pile& playerCards2Before = gameState.GetPlayerCards(1);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        gameState.TakePile(0);
        EXPECT_EQ(3, gameState.GetNumberOfPiles());
        Pile playerCards1After = gameState.GetPlayerCards(0);
        EXPECT_EQ(playerCards1Before, playerCards1After);
        Pile playerCards2After = gameState.GetPlayerCards(1);
        EXPECT_EQ(playerCards2Before, playerCards2After);
        Pile playerCards3After = gameState.GetPlayerCards(2);
        EXPECT_EQ(3, playerCards3After.GetNumberOfCards());
        EXPECT_EQ(1, playerCards3After.GetCardCount(Card::SNAKE));
        EXPECT_EQ(2, playerCards3After.GetCardCount(Card::ROOSTER));
        const set<unsigned int>& playersThatHaveTaken = gameState.GetPlayersThatHaveTaken();
        EXPECT_TRUE(playersThatHaveTaken.find(0) == playersThatHaveTaken.end());
        EXPECT_TRUE(playersThatHaveTaken.find(1) == playersThatHaveTaken.end());
        EXPECT_TRUE(playersThatHaveTaken.find(2) != playersThatHaveTaken.end());
        EXPECT_EQ(0, gameState.GetCurrentPlayer());
    }

    TEST(GameStateTest, CanTakeTripleCardPile)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(3, engine);
        const Pile& playerCards2Before = gameState.GetPlayerCards(1);
        const Pile& playerCards3Before = gameState.GetPlayerCards(2);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        gameState.TakePile(0);
        EXPECT_EQ(3, gameState.GetNumberOfPiles());
        Pile playerCards2After = gameState.GetPlayerCards(1);
        EXPECT_EQ(playerCards2Before, playerCards2After);
        Pile playerCards3After = gameState.GetPlayerCards(2);
        EXPECT_EQ(playerCards3Before, playerCards3After);
        Pile playerCards1After = gameState.GetPlayerCards(0);
        EXPECT_EQ(4, playerCards1After.GetNumberOfCards());
        EXPECT_EQ(2, playerCards1After.GetCardCount(Card::RAT));
        EXPECT_EQ(2, playerCards1After.GetCardCount(Card::ROOSTER));
        const set<unsigned int>& playersThatHaveTaken = gameState.GetPlayersThatHaveTaken();
        EXPECT_TRUE(playersThatHaveTaken.find(0) != playersThatHaveTaken.end());
        EXPECT_TRUE(playersThatHaveTaken.find(1) == playersThatHaveTaken.end());
        EXPECT_TRUE(playersThatHaveTaken.find(2) == playersThatHaveTaken.end());
        EXPECT_EQ(1, gameState.GetCurrentPlayer());
    }

    TEST(GameStateTest, CanTakeMultipleTimes)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(4, engine);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0); 
        gameState.TakePile(0); // 1
        gameState.DrawCard();
        gameState.PlaceCardOnPile(2);
        gameState.TakePile(2); // 3
        EXPECT_EQ(4, gameState.GetNumberOfPiles());
        const set<unsigned int>& playersThatHaveTaken = gameState.GetPlayersThatHaveTaken();
        EXPECT_TRUE(playersThatHaveTaken.find(0) == playersThatHaveTaken.end());
        EXPECT_TRUE(playersThatHaveTaken.find(1) != playersThatHaveTaken.end());
        EXPECT_TRUE(playersThatHaveTaken.find(2) == playersThatHaveTaken.end());
        EXPECT_TRUE(playersThatHaveTaken.find(3) != playersThatHaveTaken.end());
    }
    
    TEST(GameStateTest, CurrentPlayerRespectsTaken)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(4, engine);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0); 
        gameState.TakePile(0); // 1
        gameState.DrawCard();
        gameState.PlaceCardOnPile(2);
        gameState.TakePile(2); // 3
        EXPECT_EQ(0, gameState.GetCurrentPlayer());
        gameState.DrawCard();
        gameState.PlaceCardOnPile(1);
        EXPECT_EQ(2, gameState.GetCurrentPlayer());
        gameState.DrawCard();
        gameState.PlaceCardOnPile(1);
        EXPECT_EQ(0, gameState.GetCurrentPlayer());
    }

    TEST(GameStateTest, CanTakeFinalPileTwoPlayerGame)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(2, engine);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        gameState.TakePile(0); // 1
        gameState.DrawCard();
        gameState.PlaceCardOnPile(1);
        ASSERT_EQ(0, gameState.GetCurrentPlayer());
        gameState.TakePile(1);
        const set<unsigned int>& playersThatHaveTaken = gameState.GetPlayersThatHaveTaken();
        EXPECT_TRUE(playersThatHaveTaken.find(0) == playersThatHaveTaken.end());
        EXPECT_TRUE(playersThatHaveTaken.find(1) == playersThatHaveTaken.end());
        EXPECT_EQ(3, gameState.GetNumberOfPiles());
        for (unsigned int currentPile = 0; currentPile < gameState.GetNumberOfPiles(); ++currentPile)
        {
            const Pile& pile = gameState.GetPile(currentPile);
            EXPECT_EQ(0, pile.GetNumberOfCards());
        }
        EXPECT_EQ(0, gameState.GetCurrentPlayer());
        EXPECT_EQ(52, gameState.GetDeckSize());
    }
    
    TEST(GameStateTest, CannotTakeFromEmptyPile)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(3, engine);
        EXPECT_THROW(gameState.TakePile(0), CannotTakeEmptyPileError);
        EXPECT_THROW(gameState.TakePile(1), CannotTakeEmptyPileError);
        EXPECT_THROW(gameState.TakePile(2), CannotTakeEmptyPileError);
    }

    TEST(GameStateTest, CannotTakeWhenCardDrawn)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(4, engine);
        gameState.DrawCard();
        gameState.PlaceCardOnPile(0);
        gameState.DrawCard();
        EXPECT_THROW(gameState.TakePile(0), CannotTakeWhenCardDrawnError);
    }

    TEST(GameStateTest, FinalRoundCanBeReachedThreePlusPlayers)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(3, engine);
        unsigned int currentPile = 0;
        while (!gameState.IsFinalRound())
        {
            if (gameState.GetNumberOfCardsInPile(currentPile) > 0)
            {
                gameState.TakePile(currentPile);
                if (++currentPile >= gameState.GetNumberOfPiles())
                {
                    currentPile = 0;
                }
            }
            else
            {
                gameState.DrawCard();
                gameState.PlaceCardOnPile(currentPile);
            }
        }
        EXPECT_TRUE(gameState.IsFinalRound());
        EXPECT_EQ(gameState.FINAL_ROUND_AMOUNT - 1, gameState.GetDeckSize());
    }

    TEST(GameStateTest, FinalRoundCanBeReachedTwoPlayers)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(2, engine);
        unsigned int currentPile = 0;
        while (!gameState.IsFinalRound())
        {
            if (gameState.GetNumberOfCardsInPile(currentPile) > 0)
            {
                gameState.TakePile(currentPile);
                if (++currentPile >= gameState.GetNumberOfPiles())
                {
                    currentPile = 0;
                }
            }
            else
            {
                gameState.DrawCard();
                gameState.PlaceCardOnPile(currentPile);
            }
        }
        EXPECT_TRUE(gameState.IsFinalRound());
        EXPECT_EQ(gameState.FINAL_ROUND_AMOUNT - 1, gameState.GetDeckSize());
    }

    TEST(GameStateTest, GameOverCanBeReachedThreePlusPlayers)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(4, engine);
        unsigned int currentPile = 0;
        while (!gameState.IsGameOver())
        {
            if (gameState.GetNumberOfCardsInPile(currentPile) > 0)
            {
                gameState.TakePile(currentPile);
                if (++currentPile >= gameState.GetNumberOfPiles())
                {
                    currentPile = 0;
                }
            }
            else
            {
                gameState.DrawCard();
                gameState.PlaceCardOnPile(currentPile);
            }
        }
        EXPECT_TRUE(gameState.IsGameOver());
        ASSERT_THROW(gameState.DrawCard(), GameOverError);
        ASSERT_THROW(gameState.PlaceCardOnPile(0), GameOverError);
        ASSERT_THROW(gameState.TakePile(0), GameOverError);
    }

    TEST(GameStateTest, GameOverCanBeReachedTwoPlayers)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(2, engine);
        unsigned int currentPile = 0;
        while (!gameState.IsGameOver())
        {
            if (gameState.GetNumberOfCardsInPile(currentPile) > 0)
            {
                gameState.TakePile(currentPile);
                if (++currentPile >= gameState.GetNumberOfPiles())
                {
                    currentPile = 0;
                }
            }
            else
            {
                gameState.DrawCard();
                gameState.PlaceCardOnPile(currentPile);
            }
        }
        EXPECT_TRUE(gameState.IsGameOver());
        ASSERT_THROW(gameState.DrawCard(), GameOverError);
        ASSERT_THROW(gameState.PlaceCardOnPile(0), GameOverError);
        ASSERT_THROW(gameState.TakePile(0), GameOverError);
    }
}
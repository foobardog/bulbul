#include <ctime>
#include <random>
#include "gtest\gtest.h"
#include "engine\GameState.h"
#include "engine\Player.h"

using std::default_random_engine;

namespace bulbul
{
    TEST(PlayerTest, MaxPilePlayerCanPlayFullGameThreePlayerPlusGame)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(5, engine);
        MaxPilePlayer<default_random_engine> player(gameState);

        while (!gameState.IsGameOver())
        {
            player.MakeMove();
        }
        EXPECT_TRUE(gameState.IsGameOver());
    }
    
    TEST(PlayerTest, MaxPilePlayerCanPlayFullGameTwoPlayerGame)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(2, engine);
        MaxPilePlayer<default_random_engine> player(gameState);

        while (!gameState.IsGameOver())
        {
            player.MakeMove();
        }
        EXPECT_TRUE(gameState.IsGameOver());
    }
    
    TEST(PlayerTest, MaxPilePlayerCanPlayFullGameNonDeterministic)
    {
        default_random_engine engine;
        engine.seed(static_cast<unsigned long>(time(nullptr)));
        GameState<default_random_engine> gameState(5, engine);
        MaxPilePlayer<default_random_engine> player(gameState);

        while (!gameState.IsGameOver())
        {
            player.MakeMove();
        }
        EXPECT_TRUE(gameState.IsGameOver());
    }

    TEST(PlayerTest, RandomPlayerCanPlayFullGameThreePlayerPlusGame)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(5, engine);
        RandomPlayer<default_random_engine> player(gameState, engine);

        while (!gameState.IsGameOver())
        {
            player.MakeMove();
        }
        EXPECT_TRUE(gameState.IsGameOver());
    }
    
    TEST(PlayerTest, RandomPlayerCanPlayFullGameTwoPlayerGame)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(2, engine);
        RandomPlayer<default_random_engine> player(gameState, engine);

        while (!gameState.IsGameOver())
        {
            player.MakeMove();
        }
        EXPECT_TRUE(gameState.IsGameOver());
    }
    
    TEST(PlayerTest, RandomPlayerCanPlayFullGameNonDeterministic)
    {
        default_random_engine engine;
        engine.seed(static_cast<unsigned long>(time(nullptr)));
        GameState<default_random_engine> gameState(5, engine);
        RandomPlayer<default_random_engine> player(gameState, engine);

        while (!gameState.IsGameOver())
        {
            player.MakeMove();
        }
        EXPECT_TRUE(gameState.IsGameOver());
    }

    TEST(PlayerTest, ImmediatePlayerCanPlayFullGameThreePlayerPlusGame)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(5, engine);
        ImmediatePlayer<default_random_engine> player(gameState);

        while (!gameState.IsGameOver())
        {
            player.MakeMove();
        }
        EXPECT_TRUE(gameState.IsGameOver());
    }
    
    TEST(PlayerTest, ImmediatePlayerCanPlayFullGameTwoPlayerGame)
    {
        default_random_engine engine(13);
        GameState<default_random_engine> gameState(2, engine);
        ImmediatePlayer<default_random_engine> player(gameState);

        while (!gameState.IsGameOver())
        {
            player.MakeMove();
        }
        EXPECT_TRUE(gameState.IsGameOver());
    }
    
    TEST(PlayerTest, ImmediatePlayerCanPlayFullGameNonDeterministic)
    {
        default_random_engine engine;
        engine.seed(static_cast<unsigned long>(time(nullptr)));
        GameState<default_random_engine> gameState(5, engine);
        ImmediatePlayer<default_random_engine> player(gameState);

        while (!gameState.IsGameOver())
        {
            player.MakeMove();
        }
        EXPECT_TRUE(gameState.IsGameOver());
    }
}
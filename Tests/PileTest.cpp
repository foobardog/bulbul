#include "gtest\gtest.h"
#include "engine\Card.h"
#include "engine\Pile.h"

namespace bulbul
{
    TEST(PileTest, CanCreateAndUseUnboundedPile)
    {
        Pile pile;
        EXPECT_EQ(0, pile.GetNumberOfCards());
        EXPECT_EQ(0, pile.GetNumberOfCardTypes());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            EXPECT_EQ(0, pile.GetCardCount(card));
        }
        EXPECT_EQ(0, pile.GetCardTypes().size());

        pile.AddCard(Card::JOKER);
        EXPECT_EQ(1, pile.GetNumberOfCards());
        EXPECT_EQ(1, pile.GetNumberOfCardTypes());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            unsigned int expectedCards = card == Card::JOKER ? 1 : 0;
            EXPECT_EQ(expectedCards, pile.GetCardCount(card));
        }
        set<Card> cardTypes1 = pile.GetCardTypes();
        EXPECT_EQ(1, cardTypes1.size());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER)
            {
                EXPECT_TRUE(cardTypes1.find(card) != cardTypes1.end());
            }
            else
            {
                EXPECT_TRUE(cardTypes1.find(card) == cardTypes1.end());
            }
        }
        
        pile.AddCard(Card::RAT);
        EXPECT_EQ(2, pile.GetNumberOfCards());
        EXPECT_EQ(2, pile.GetNumberOfCardTypes());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            unsigned int expectedCards = (card == Card::JOKER || card == Card::RAT) ? 1 : 0;
            EXPECT_EQ(expectedCards, pile.GetCardCount(card));
        }
        set<Card> cardTypes2 = pile.GetCardTypes();
        EXPECT_EQ(2, cardTypes2.size());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER || card == Card::RAT)
            {
                EXPECT_TRUE(cardTypes2.find(card) != cardTypes2.end());
            }
            else
            {
                EXPECT_TRUE(cardTypes2.find(card) == cardTypes2.end());
            }
        }
        
        pile.AddCard(Card::RAT);
        EXPECT_EQ(3, pile.GetNumberOfCards());
        EXPECT_EQ(2, pile.GetNumberOfCardTypes());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER)
            {
                EXPECT_EQ(1, pile.GetCardCount(card));
            }
            else if (card == Card::RAT)
            {
                EXPECT_EQ(2, pile.GetCardCount(card));
            }
            else
            {
                EXPECT_EQ(0, pile.GetCardCount(card));
            }
        }
        set<Card> cardTypes3 = pile.GetCardTypes();
        EXPECT_EQ(2, cardTypes3.size());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER || card == Card::RAT)
            {
                EXPECT_TRUE(cardTypes3.find(card) != cardTypes3.end());
            }
            else
            {
                EXPECT_TRUE(cardTypes3.find(card) == cardTypes3.end());
            }
        }
        
        pile.AddCard(Card::PLUS_TWO);
        EXPECT_EQ(4, pile.GetNumberOfCards());
        EXPECT_EQ(3, pile.GetNumberOfCardTypes());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER || card == Card::PLUS_TWO)
            {
                EXPECT_EQ(1, pile.GetCardCount(card));
            }
            else if (card == Card::RAT)
            {
                EXPECT_EQ(2, pile.GetCardCount(card));
            }
            else
            {
                EXPECT_EQ(0, pile.GetCardCount(card));
            }
        }
        set<Card> cardTypes4 = pile.GetCardTypes();
        EXPECT_EQ(3, cardTypes4.size());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER || card == Card::RAT || card == Card::PLUS_TWO)
            {
                EXPECT_TRUE(cardTypes4.find(card) != cardTypes4.end());
            }
            else
            {
                EXPECT_TRUE(cardTypes4.find(card) == cardTypes4.end());
            }
        }
    }

    TEST(PileTest, CanCreateAndUseBoundedPile)
    {
        BoundedPile pile(4);
        EXPECT_EQ(4, pile.GetCapacity());
        EXPECT_FALSE(pile.IsFull());
        EXPECT_EQ(0, pile.GetNumberOfCards());
        EXPECT_EQ(0, pile.GetNumberOfCardTypes());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            EXPECT_EQ(0, pile.GetCardCount(card));
        }
        EXPECT_EQ(0, pile.GetCardTypes().size());

        pile.AddCard(Card::JOKER);
        EXPECT_EQ(4, pile.GetCapacity());
        EXPECT_FALSE(pile.IsFull());
        EXPECT_EQ(1, pile.GetNumberOfCards());
        EXPECT_EQ(1, pile.GetNumberOfCardTypes());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            unsigned int expectedCards = card == Card::JOKER ? 1 : 0;
            EXPECT_EQ(expectedCards, pile.GetCardCount(card));
        }
        set<Card> cardTypes1 = pile.GetCardTypes();
        EXPECT_EQ(1, cardTypes1.size());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER)
            {
                EXPECT_TRUE(cardTypes1.find(card) != cardTypes1.end());
            }
            else
            {
                EXPECT_TRUE(cardTypes1.find(card) == cardTypes1.end());
            }
        }
        
        pile.AddCard(Card::RAT);
        EXPECT_EQ(4, pile.GetCapacity());
        EXPECT_FALSE(pile.IsFull());
        EXPECT_EQ(2, pile.GetNumberOfCards());
        EXPECT_EQ(2, pile.GetNumberOfCardTypes());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            unsigned int expectedCards = (card == Card::JOKER || card == Card::RAT) ? 1 : 0;
            EXPECT_EQ(expectedCards, pile.GetCardCount(card));
        }
        set<Card> cardTypes2 = pile.GetCardTypes();
        EXPECT_EQ(2, cardTypes2.size());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER || card == Card::RAT)
            {
                EXPECT_TRUE(cardTypes2.find(card) != cardTypes2.end());
            }
            else
            {
                EXPECT_TRUE(cardTypes2.find(card) == cardTypes2.end());
            }
        }
        
        pile.AddCard(Card::RAT);
        EXPECT_EQ(4, pile.GetCapacity());
        EXPECT_FALSE(pile.IsFull());
        EXPECT_EQ(3, pile.GetNumberOfCards());
        EXPECT_EQ(2, pile.GetNumberOfCardTypes());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER)
            {
                EXPECT_EQ(1, pile.GetCardCount(card));
            }
            else if (card == Card::RAT)
            {
                EXPECT_EQ(2, pile.GetCardCount(card));
            }
            else
            {
                EXPECT_EQ(0, pile.GetCardCount(card));
            }
        }
        set<Card> cardTypes3 = pile.GetCardTypes();
        EXPECT_EQ(2, cardTypes3.size());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER || card == Card::RAT)
            {
                EXPECT_TRUE(cardTypes3.find(card) != cardTypes3.end());
            }
            else
            {
                EXPECT_TRUE(cardTypes3.find(card) == cardTypes3.end());
            }
        }
        
        pile.AddCard(Card::PLUS_TWO);
        EXPECT_EQ(4, pile.GetCapacity());
        EXPECT_TRUE(pile.IsFull());
        EXPECT_EQ(4, pile.GetNumberOfCards());
        EXPECT_EQ(3, pile.GetNumberOfCardTypes());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER || card == Card::PLUS_TWO)
            {
                EXPECT_EQ(1, pile.GetCardCount(card));
            }
            else if (card == Card::RAT)
            {
                EXPECT_EQ(2, pile.GetCardCount(card));
            }
            else
            {
                EXPECT_EQ(0, pile.GetCardCount(card));
            }
        }
        set<Card> cardTypes4 = pile.GetCardTypes();
        EXPECT_EQ(3, cardTypes4.size());
        for (Card card = Card::FIRST_CARD; card != Card::PAST_ALL_CARDS; ++card)
        {
            if (card == Card::JOKER || card == Card::RAT || card == Card::PLUS_TWO)
            {
                EXPECT_TRUE(cardTypes4.find(card) != cardTypes4.end());
            }
            else
            {
                EXPECT_TRUE(cardTypes4.find(card) == cardTypes4.end());
            }
        }

        EXPECT_THROW(pile.AddCard(Card::PLUS_TWO), CannotPlaceOnFullPileError);
    }

    TEST(PileTest, CanScoreHandsWithAnimalCards)
    {
        Pile pile;
        EXPECT_EQ(0, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(0, pile.ScorePile(Pile::maximumScoringMap));

        pile.ClearPile();
        pile.AddCard(Card::RAT);
        EXPECT_EQ(1, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(1, pile.ScorePile(Pile::maximumScoringMap));
       
        pile.ClearPile();
        pile.AddCard(Card::RABBIT);
        pile.AddCard(Card::RABBIT);
        EXPECT_EQ(3, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(4, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::SNAKE);
        pile.AddCard(Card::SNAKE);
        pile.AddCard(Card::SNAKE);
        EXPECT_EQ(6, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(8, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::SHEEP);
        pile.AddCard(Card::SHEEP);
        pile.AddCard(Card::SHEEP);
        pile.AddCard(Card::SHEEP);
        EXPECT_EQ(10, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(7, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::MONKEY);
        pile.AddCard(Card::MONKEY);
        pile.AddCard(Card::MONKEY);
        pile.AddCard(Card::MONKEY);
        pile.AddCard(Card::MONKEY);
        EXPECT_EQ(15, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(6, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::ROOSTER);
        pile.AddCard(Card::ROOSTER);
        pile.AddCard(Card::ROOSTER);
        pile.AddCard(Card::ROOSTER);
        pile.AddCard(Card::ROOSTER);
        pile.AddCard(Card::ROOSTER);
        EXPECT_EQ(21, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(5, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        EXPECT_EQ(21, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(5, pile.ScorePile(Pile::maximumScoringMap));
    }

    TEST(PileTest, CanScoreHandsWithPlusTwos)
    {
        Pile pile;
        pile.AddCard(Card::PLUS_TWO);
        EXPECT_EQ(2, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(2, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::PLUS_TWO);
        EXPECT_EQ(4, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(4, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::PLUS_TWO);
        EXPECT_EQ(6, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(6, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::PLUS_TWO);
        EXPECT_EQ(8, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(10, pile.ScorePile(Pile::maximumScoringMap));

        pile.ClearPile();
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::RAT);
        pile.AddCard(Card::RABBIT);
        EXPECT_EQ(13, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(13, pile.ScorePile(Pile::maximumScoringMap));
    }

    TEST(PileTest, CanScoreHandsWithJokers)
    {
        Pile pile;
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::RAT);
        pile.AddCard(Card::RAT);
        pile.AddCard(Card::RAT);
        pile.AddCard(Card::RAT);
        EXPECT_EQ(31, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(14, pile.ScorePile(Pile::maximumScoringMap));

        pile.ClearPile();
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::RAT);
        EXPECT_EQ(7, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(9, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::RAT);
        EXPECT_EQ(11, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(12, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::RAT);
        EXPECT_EQ(16, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(16, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::RAT);
        EXPECT_EQ(24, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(16, pile.ScorePile(Pile::maximumScoringMap));

        pile.ClearPile();
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::JOKER);
        EXPECT_EQ(6, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(8, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::JOKER);
        EXPECT_EQ(3, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(4, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::JOKER);
        EXPECT_EQ(1, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(1, pile.ScorePile(Pile::maximumScoringMap));

        pile.ClearPile();
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::PLUS_TWO);
        pile.AddCard(Card::JOKER);
        pile.AddCard(Card::DOG);
        pile.AddCard(Card::RAT);
        pile.AddCard(Card::RABBIT);
        EXPECT_EQ(15, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(16, pile.ScorePile(Pile::maximumScoringMap));
    }

    TEST(PileTest, CanScoreHandsWithNegativeCards)
    {
        Pile pile;
        pile.AddCard(Card::RAT);
        pile.AddCard(Card::RABBIT);
        pile.AddCard(Card::SNAKE);
        pile.AddCard(Card::SHEEP);
        EXPECT_EQ(2, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(2, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::RAT);
        pile.AddCard(Card::RABBIT);
        pile.AddCard(Card::SNAKE);
        pile.AddCard(Card::SHEEP);
        pile.AddCard(Card::ROOSTER);
        EXPECT_EQ(1, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(1, pile.ScorePile(Pile::maximumScoringMap));
        
        pile.ClearPile();
        pile.AddCard(Card::RAT);
        pile.AddCard(Card::RABBIT);
        pile.AddCard(Card::SNAKE);
        pile.AddCard(Card::SHEEP);
        pile.AddCard(Card::ROOSTER);
        pile.AddCard(Card::DOG);
        EXPECT_EQ(0, pile.ScorePile(Pile::additiveScoringMap));
        EXPECT_EQ(0, pile.ScorePile(Pile::maximumScoringMap));
    }
}
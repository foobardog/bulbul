#pragma once
#include <map>
#include <set>
#include <string>
#include "Card.h"

using std::map;
using std::set;

namespace bulbul
{
    typedef map<Card, unsigned int> CardCountMap;
    typedef map<unsigned int, unsigned int> ScoringMap;

    class Pile
    {
    public:
        const static ScoringMap additiveScoringMap;
        const static ScoringMap maximumScoringMap;
        bool isTaken;

        Pile() : isTaken(false) {}

        unsigned int GetCardCount(Card cardType) const;
        unsigned int GetNumberOfCards() const;
        unsigned int GetNumberOfCardTypes() const;
        const set<Card>& GetCardTypes() const;
        bool operator==(const Pile& rhs) const;
        int ScorePile(ScoringMap scoringMap) const;
        virtual void AddCard(Card cardType);
        void ClearPile();
    private:
        CardCountMap cardCounts;
        set<Card> cardTypes;
        int ScoreWithoutJokers(CardCountMap cardCounts, ScoringMap scoringMap) const;
    };

    ScoringMap CreateAdditiveScoringMap();

    ScoringMap CreateMaximumScoringMap();

    class BoundedPile : public Pile
    {
    public:
        BoundedPile(unsigned int newSize) : size(newSize) {}
        unsigned int GetCapacity() const;
        bool IsFull() const;
        virtual void AddCard(Card cardType) override;

    private:
        unsigned int size;
    };

    class CannotPlaceOnFullPileError : logic_error
    {
    public:
        CannotPlaceOnFullPileError() : logic_error("Cannot place on full pile.") {}
        CannotPlaceOnFullPileError(const string& message) : logic_error(message) {}
    };

    class CannotPlaceOnTakenPileError : logic_error
    {
    public:
        CannotPlaceOnTakenPileError() : logic_error("Cannot place on taken pile.") {}
        CannotPlaceOnTakenPileError(const string& message) : logic_error(message) {}
    };
}
#pragma once
#include <algorithm>
#include <map>
#include <memory>
#include <numeric>
#include <set>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include "gtest/gtest_prod.h"
#include "Card.h"
#include "Pile.h"

using std::accumulate;
using std::all_of;
using std::logic_error;
using std::map;
using std::move;
using std::pair;
using std::set;
using std::shuffle;
using std::to_string;
using std::unique_ptr;
using std::vector;

namespace bulbul
{
    class CannotDrawTwiceWithoutPlacingError : logic_error
    {
    public:
        CannotDrawTwiceWithoutPlacingError() : logic_error("Cannot draw twice without placing.") {}
        CannotDrawTwiceWithoutPlacingError(const string& message) : logic_error(message) {}
    };

    class CannotPlaceWhenNoCardDrawnError : logic_error
    {
    public:
        CannotPlaceWhenNoCardDrawnError() : logic_error("Cannot place when no card is drawn.") {}
        CannotPlaceWhenNoCardDrawnError(const string& message) : logic_error(message) {}
    };

    class CannotTakeEmptyPileError : logic_error
    {
    public:
        CannotTakeEmptyPileError() : logic_error("Cannot take empty pile.") {}
        CannotTakeEmptyPileError(const string& message) : logic_error(message) {}
    };

    class CannotTakeTakenPileError : logic_error
    {
    public:
        CannotTakeTakenPileError() : logic_error("Cannot take taken pile.") {}
        CannotTakeTakenPileError(const string& message) : logic_error(message) {}
    };

    class CannotTakeWhenCardDrawnError : logic_error
    {
    public:
        CannotTakeWhenCardDrawnError() : logic_error("Cannot take when card is drawn.") {}
        CannotTakeWhenCardDrawnError(const string& message) : logic_error(message) {}
    };

    class GameOverError : logic_error
    {
    public:
        GameOverError() : logic_error("Game is over.") {}
        GameOverError(const string& message) : logic_error(message) {}
    };

    template <typename Engine>
    class GameState
    {
    public:
        static const unsigned int FIRST_PLAYER;
        static const Card NO_CARD_DRAWN;
        static const unsigned int CARDS_PER_ANIMAL_CARD_SET;
        static const unsigned int JOKER_CARDS_TO_ADD;
        static const unsigned int PLUS_TWO_CARDS_TO_ADD;
        static const Card THREE_PLAYER_REMOVED_CARD;
        static const Card TWO_PLAYER_REMOVED_CARD;
        static const unsigned int TWO_PLAYER_TOTAL_STARTING_CARD_COUNT;
        static const unsigned int TWO_PLAYER_STARTING_CARD_COUNT;
        static const unsigned int TWO_PLAYER_STARTING_PILES;
        static const unsigned int THREE_PLAYER_PLUS_CARDS_PER_PILE;
        static const unsigned int FINAL_ROUND_AMOUNT;

        GameState(unsigned int numberOfPlayers, Engine& newEngine)
        {
            engine = newEngine;
            this->numberOfPlayers = numberOfPlayers;
            currentPlayer = FIRST_PLAYER;
            gameOver = false;
            drawnCard = NO_CARD_DRAWN;

            CreatePlayerCardsMaps();

            set<Card> animalCardsToAdd;
            DetermineAnimalCardsToAdd(animalCardsToAdd);

            set<Card> startingPlayerCards;
            DetermineStartingPlayerCards(animalCardsToAdd, startingPlayerCards);

            CreateDeck(animalCardsToAdd, startingPlayerCards);
            AddPlayerCards(startingPlayerCards);
            CreatePiles();
        }

        Card DrawCard() 
        {
            if (IsGameOver())
            {
                throw GameOverError();
            }

            if (IsCardDrawn())
            {
                throw CannotDrawTwiceWithoutPlacingError();
            }
            drawnCard = deck.back();
            deck.pop_back();
            return drawnCard;
        }

        void PlaceCardOnPile(unsigned int pile) 
        {
            if (IsGameOver())
            {
                throw GameOverError();
            }

            if (!IsCardDrawn())
            {
                throw CannotPlaceWhenNoCardDrawnError();
            }

            piles[pile].AddCard(drawnCard);
            drawnCard = NO_CARD_DRAWN;
            IncrementCurrentPlayer();
        }

        void TakePile(unsigned int pileIndex) 
        {
            if (IsGameOver())
            {
                throw GameOverError();
            }

            if (IsCardDrawn())
            {
                throw CannotTakeWhenCardDrawnError();
            }

            if (GetNumberOfCardsInPile(pileIndex) == 0)
            {
                throw CannotTakeEmptyPileError();
            }

            if (IsPileTaken(pileIndex))
            {
                throw CannotTakeTakenPileError();
            }

            set<Card> pileCardTypes = piles[pileIndex].GetCardTypes();
            for (Card cardType : pileCardTypes)
            {
                for (unsigned int currentCard = 0; currentCard < piles[pileIndex].GetCardCount(cardType); ++currentCard)
                {
                    playerCards[GetCurrentPlayer()].AddCard(cardType);
                }
            }

            piles[pileIndex].isTaken = true;
            piles[pileIndex].ClearPile();
            playersThatHaveTaken.emplace(currentPlayer);

            if (playersThatHaveTaken.size() != numberOfPlayers)
            {
                IncrementCurrentPlayer();
            }
            else
            {
                if (IsFinalRound())
                {
                    gameOver = true;
                }
                else
                {
                    for (BoundedPile& pile : piles)
                    {
                        pile.isTaken = false;
                        pile.ClearPile();
                    }
                    playersThatHaveTaken.clear();
                }
            }

        }

        unsigned int GetNumberOfPlayers() const
        {
            return numberOfPlayers;
        }

        unsigned int GetCurrentPlayer() const
        {
            return currentPlayer;
        }

        bool IsFinalRound() const
        {
            return deck.size() < FINAL_ROUND_AMOUNT;
        }

        bool IsGameOver() const
        {
            return gameOver;
        }

        bool IsCardDrawn() const
        {
            return drawnCard != NO_CARD_DRAWN;
        }

        Card GetDrawnCard() const
        {
            return drawnCard;
        }

        unsigned int GetDeckSize() const
        {
            return deck.size();
        }

        unsigned int GetNumberOfPiles() const
        {
            return piles.size();
        }

        const BoundedPile& GetPile(unsigned int pile) const
        {
            return piles[pile];
        }

        const vector<BoundedPile>& GetPiles() const
        {
            return piles;
        }

        const bool AllPilesFull() const
        {
            return all_of(piles.begin(), piles.end(), [] (BoundedPile pile) { return pile.isTaken || pile.IsFull(); });
        }

        unsigned int GetMaximumCardsPerPile(unsigned int pile) const
        {
            return piles[pile].GetCapacity();
        }

        unsigned int GetNumberOfCardsInPile(unsigned int pileIndex) const
        {
            return piles[pileIndex].GetNumberOfCards();
        }

        bool IsPileTaken(unsigned int pileIndex) const
        {
            return piles[pileIndex].isTaken;
        }

        const Pile& GetPlayerCards(unsigned int player) const
        {
            return playerCards[player];
        }

        const vector<Pile>& GetAllPlayerCards() const
        {
            return playerCards;
        }

        const set<unsigned int>& GetPlayersThatHaveTaken() const
        {
            return playersThatHaveTaken;
        }

    private:
        Engine engine;
        unsigned int numberOfPlayers;
        unsigned int currentPlayer;
        bool gameOver;
        Card drawnCard;
        vector<Card> deck;
        vector<BoundedPile> piles;
        vector<Pile> playerCards;
        set<unsigned int> playersThatHaveTaken;

        FRIEND_TEST(GameStateTest, CanCreateTwoPlayerGame);
        FRIEND_TEST(GameStateTest, CanCreateThreePlayerGame);
        FRIEND_TEST(GameStateTest, CanCreateFourPlayerGame);
        FRIEND_TEST(GameStateTest, CanCreateFivePlayerGame);
        FRIEND_TEST(GameStateTest, CardCanBeDrawn);
        FRIEND_TEST(GameStateTest, CardCanBePlaced);
        const vector<Card>& GetDeck()
        {
            return deck;
        }

        void CreatePlayerCardsMaps()
        {
            for (unsigned int currentPlayer = 0; currentPlayer < numberOfPlayers; currentPlayer++)
            {
                playerCards.emplace_back();
            }
        }

        void DetermineAnimalCardsToAdd(set<Card>& animalCardsToAdd)
        {
            for (Card card = Card::FIRST_ANIMAL_CARD; card <= Card::LAST_ANIMAL_CARD; ++card)
            {
                animalCardsToAdd.insert(card);
            }

            if (numberOfPlayers <= 3) 
            {
                animalCardsToAdd.erase(animalCardsToAdd.find(THREE_PLAYER_REMOVED_CARD));
            }

            if (numberOfPlayers == 2)
            {
                animalCardsToAdd.erase(animalCardsToAdd.find(TWO_PLAYER_REMOVED_CARD));
            }
        }

        void DetermineStartingPlayerCards(const set<Card>& animalCardsToAdd, set<Card>& startingPlayerCards)
        {
            unsigned int startingPlayerCardsCount = numberOfPlayers == 2 ? TWO_PLAYER_TOTAL_STARTING_CARD_COUNT : numberOfPlayers;
            auto currentCard = animalCardsToAdd.begin();
            for (unsigned int cardsAdded = 0; cardsAdded < startingPlayerCardsCount; cardsAdded++)
            {
                startingPlayerCards.insert(*currentCard);
                ++currentCard;
            }
        }

        void CreateDeck(const set<Card>& animalCardsToAdd, const set<Card>& startingPlayerCards)
        {
            for (auto currentAnimalCardToAdd = animalCardsToAdd.begin(); 
                currentAnimalCardToAdd != animalCardsToAdd.end(); 
                ++currentAnimalCardToAdd)
            {
                bool isStartingPlayerCard = (startingPlayerCards.find(*currentAnimalCardToAdd) != startingPlayerCards.end());
                unsigned int cardsToAdd = isStartingPlayerCard ? CARDS_PER_ANIMAL_CARD_SET - 1 : CARDS_PER_ANIMAL_CARD_SET;
                for (unsigned int currentCard = 0; currentCard < cardsToAdd; currentCard++)
                {
                    deck.push_back(*currentAnimalCardToAdd);
                }
            }

            for (unsigned int currentCard = 0; currentCard < JOKER_CARDS_TO_ADD; currentCard++)
            {
                deck.push_back(Card::JOKER);
            }

            for (unsigned int currentCard = 0; currentCard < PLUS_TWO_CARDS_TO_ADD; currentCard++)
            {
                deck.push_back(Card::PLUS_TWO);
            }

            shuffle(deck.begin(), deck.end(), engine);
        }

        void AddPlayerCards(const set<Card>& startingPlayerCards)
        {
            int currentPlayerToAddTo = 0;
            for (auto currentStartingCardToAdd = startingPlayerCards.begin(); currentStartingCardToAdd != startingPlayerCards.end(); ++currentStartingCardToAdd)
            {
                playerCards[currentPlayerToAddTo].AddCard(*currentStartingCardToAdd);
                if (numberOfPlayers != 2 || playerCards[currentPlayerToAddTo].GetNumberOfCards() == TWO_PLAYER_STARTING_CARD_COUNT)
                {
                    ++currentPlayerToAddTo;
                }
            }
        }

        void CreatePiles()
        {
            if (numberOfPlayers == 2)
            {
                for (unsigned int currentPile = 0; currentPile < TWO_PLAYER_STARTING_PILES; currentPile++)
                {
                    piles.emplace_back(currentPile + 1);
                }
            }
            else
            {
                for (unsigned int currentPile = 0; currentPile < numberOfPlayers; currentPile++)
                {
                    piles.emplace_back(THREE_PLAYER_PLUS_CARDS_PER_PILE);
                }
            }
        }

        void IncrementCurrentPlayer()
        {
            unsigned int nextPlayer = currentPlayer;
            do
            {
                nextPlayer = (nextPlayer + 1) % numberOfPlayers;
            } while(playersThatHaveTaken.find(nextPlayer) != playersThatHaveTaken.end());
            currentPlayer = nextPlayer;
        }
    };

    template <typename Engine>
    const unsigned int GameState<Engine>::FIRST_PLAYER = 0;
    template <typename Engine>
    const Card GameState<Engine>::NO_CARD_DRAWN = Card::PAST_ALL_CARDS;
    template <typename Engine>
    const unsigned int GameState<Engine>::CARDS_PER_ANIMAL_CARD_SET = 9;
    template <typename Engine>
    const unsigned int GameState<Engine>::JOKER_CARDS_TO_ADD = 3;
    template <typename Engine>
    const unsigned int GameState<Engine>::PLUS_TWO_CARDS_TO_ADD = 10;
    template <typename Engine>
    const Card GameState<Engine>::THREE_PLAYER_REMOVED_CARD = Card::DOG;
    template <typename Engine>
    const Card GameState<Engine>::TWO_PLAYER_REMOVED_CARD = Card::ROOSTER;
    template <typename Engine>
    const unsigned int GameState<Engine>::TWO_PLAYER_TOTAL_STARTING_CARD_COUNT = 4;
    template <typename Engine>
    const unsigned int GameState<Engine>::TWO_PLAYER_STARTING_CARD_COUNT = 2;
    template <typename Engine>
    const unsigned int GameState<Engine>::TWO_PLAYER_STARTING_PILES = 3;
    template <typename Engine>
    const unsigned int GameState<Engine>::THREE_PLAYER_PLUS_CARDS_PER_PILE = 3;
    template <typename Engine>
    const unsigned int GameState<Engine>::FINAL_ROUND_AMOUNT = 15;
}
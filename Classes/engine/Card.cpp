#include "Card.h"

namespace bulbul
{
    bool IsAnimalCard(Card card)
    {
        return (Card::FIRST_ANIMAL_CARD <= card) && (Card::LAST_ANIMAL_CARD >= card);
    }
    
    bool IsSpecialCard(Card card)
    {
        return (Card::FIRST_SPECIAL_CARD <= card) && (Card::LAST_SPECIAL_CARD >= card);
    }

    bool IsPlayableCard(Card card)
    {
        return card != Card::NUMBER_OF_ALL_CARDS;
    }

	Card operator++(Card& card)
	{
		// You are allowed to increment to just past all cards so this can be used in a for loop.
		if (card == Card::PAST_ALL_CARDS)
		{
			throw IncrementedPastLastCardError();
		}
		card = static_cast<Card>(static_cast<std::underlying_type<Card>::type>(card) + 1);
		return card;
	}
}
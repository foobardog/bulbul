#pragma once
#include <stdexcept>
#include <string>

using std::logic_error;
using std::string;

namespace bulbul
{
	enum class Card
	{
		JOKER,
		PLUS_TWO,
		RAT,
		PAST_SPECIAL_CARDS = RAT,
		RABBIT,
		SNAKE,
		SHEEP,
		MONKEY,
		ROOSTER,
		DOG,
		NUMBER_OF_ALL_CARDS,
		PAST_ANIMAL_CARDS = NUMBER_OF_ALL_CARDS,
		PAST_ALL_CARDS = NUMBER_OF_ALL_CARDS,
		FIRST_CARD = JOKER,
		LAST_CARD = DOG,
		FIRST_SPECIAL_CARD = JOKER,
		LAST_SPECIAL_CARD = PLUS_TWO,
		FIRST_ANIMAL_CARD = RAT,
		LAST_ANIMAL_CARD = DOG,
		NUMBER_OF_SPECIAL_CARDS = 2,
		NUMBER_OF_ANIMAL_CARDS = 7,
	};

	class IncrementedPastLastCardError : logic_error
	{
	public:
		IncrementedPastLastCardError() : logic_error("Incremented past last card.") {}
		IncrementedPastLastCardError(const string& message) : logic_error(message) {} 
	};

    bool IsAnimalCard(Card card);

    bool IsSpecialCard(Card card);

    bool IsPlayableCard(Card card);

	Card operator++(Card& card);
}
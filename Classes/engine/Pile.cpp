#include <algorithm>
#include <limits>
#include <numeric>
#include <string>
#include <utility>
#include <vector>
#include "Card.h"
#include "Pile.h"

using std::accumulate;
using std::back_inserter;
using std::copy;
using std::equal;
using std::numeric_limits;
using std::pair;
using std::sort;
using std::to_string;
using std::vector;

namespace bulbul
{
    unsigned int Pile::GetCardCount(Card cardType) const
    {
        unsigned int numberOfCards = 0;
        if (cardCounts.find(cardType) != cardCounts.end())
        {
            numberOfCards = cardCounts.at(cardType);
        }
        return numberOfCards;
    }

    unsigned int Pile::GetNumberOfCards() const
    {
        return accumulate(cardCounts.begin(), cardCounts.end(), 0, [] (int total, const pair<Card, unsigned int>& pair) { return total + pair.second; });
    }

    unsigned int Pile::GetNumberOfCardTypes() const
    {
        return cardCounts.size();
    }

    const set<Card>& Pile::GetCardTypes() const
    {
        return cardTypes;
    }

    bool Pile::operator==(const Pile& right) const
    {
        return equal(cardCounts.begin(), cardCounts.end(), right.cardCounts.begin()) && 
            equal(cardTypes.begin(), cardTypes.end(), right.cardTypes.begin());
    }

    ScoringMap CreateAdditiveScoringMap()
    {
        ScoringMap newMap;
        newMap[0] = 0;
        newMap[1] = 1;
        newMap[2] = 3;
        newMap[3] = 6;
        newMap[4] = 10;
        newMap[5] = 15;
        newMap[6] = 21;
        return newMap;
    }
    const ScoringMap Pile::additiveScoringMap = CreateAdditiveScoringMap();

    ScoringMap CreateMaximumScoringMap()
    {
        ScoringMap newMap;
        newMap[0] = 0;
        newMap[1] = 1;
        newMap[2] = 4;
        newMap[3] = 8;
        newMap[4] = 7;
        newMap[5] = 6;
        newMap[6] = 5;
        return newMap;
    }
    const ScoringMap Pile::maximumScoringMap = CreateMaximumScoringMap();

    int Pile::ScorePile(ScoringMap scoringMap) const
    {
        CardCountMap cardCountsTemp = cardCounts;
        while (cardCountsTemp.find(Card::JOKER) != cardCountsTemp.end() && cardCountsTemp.at(Card::JOKER) > 0)
        {
            --cardCountsTemp[Card::JOKER];
            Card bestCard = Card::FIRST_ANIMAL_CARD;
            int bestScore = numeric_limits<int>::min();
            for (Card currentAnimalCard = bestCard; currentAnimalCard < Card::PAST_ANIMAL_CARDS; ++currentAnimalCard)
            {
                ++cardCountsTemp[currentAnimalCard];
                int currentScore = ScoreWithoutJokers(cardCountsTemp, scoringMap);
                if (currentScore > bestScore)
                {
                    bestScore = currentScore;
                    bestCard = currentAnimalCard;
                }
                --cardCountsTemp[currentAnimalCard];
            }
            ++cardCountsTemp[bestCard];
        }
        return ScoreWithoutJokers(cardCountsTemp, scoringMap);
    }

    void Pile::AddCard(Card cardType)
    {
        if (isTaken)
        {
            throw CannotPlaceOnTakenPileError();
        }

        ++cardCounts[cardType];
        cardTypes.insert(cardType);
    }

    void Pile::ClearPile()
    {
        cardCounts.clear();
        cardTypes.clear();
    }

    int Pile::ScoreWithoutJokers(CardCountMap cardCounts, ScoringMap scoringMap) const
    {
        int score = 0;
        vector<pair<Card, unsigned int>> cardCountVector;
        copy(cardCounts.begin(), cardCounts.end(), back_inserter<vector<pair<Card, unsigned int>>>(cardCountVector));
        sort(cardCountVector.begin(), 
             cardCountVector.end(), 
             [] (pair<Card, unsigned int>& a, pair<Card, unsigned int>& b) { return a.second > b.second; });
        unsigned int currentPair = 0;
        unsigned int currentAnimalCard = 0;
        for (; currentPair < cardCountVector.size() && currentAnimalCard < 3; ++currentPair)
        {
            Card cardType = cardCountVector.at(currentPair).first;
            unsigned int cardCount = cardCountVector.at(currentPair).second;

            if (cardType == Card::PLUS_TWO)
            {
                score += cardCount * 2;
            }
            else if (IsAnimalCard(cardType))
            {
                ++currentAnimalCard;
                score += cardCount > 6 ? scoringMap.at(6) : scoringMap.at(cardCount);
            }
        }

        for (; currentPair < cardCountVector.size(); ++currentPair)
        {
            Card cardType = cardCountVector.at(currentPair).first;
            unsigned int cardCount = cardCountVector.at(currentPair).second;

            if (cardType == Card::PLUS_TWO)
            {
                score += cardCount * 2;
            }
            else if (IsAnimalCard(cardType))
            {
                score -= cardCount > 6 ? scoringMap.at(6) : scoringMap.at(cardCount);
            }
        }
        return score;
    }
    
    unsigned int BoundedPile::GetCapacity() const
    {
        return size;
    }

    bool BoundedPile::IsFull() const
    {
        return GetNumberOfCards() == size;
    }

    void BoundedPile::AddCard(Card cardType)
    {
        if (IsFull())
        {
            throw CannotPlaceOnFullPileError("Cannot place on full pile. This pile holds " + to_string(size) + " cards.");
        }
        Pile::AddCard(cardType);
    }
}
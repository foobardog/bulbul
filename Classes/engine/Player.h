#pragma once
#include <algorithm>
#include <random>
#include <vector>
#include "GameState.h"

using std::all_of;
using std::any_of;
using std::bernoulli_distribution;
using std::find_if;
using std::logic_error;
using std::shuffle;
using std::uniform_int_distribution;
using std::vector;

namespace bulbul
{
    template <typename Engine>
    class Player
    {
    public:
        Player(GameState<Engine>& newGameState) : gameState(newGameState)
        {
        }
        virtual ~Player() {}
        virtual void MakeMove() = 0;
    protected:
        GameState<Engine>& gameState;
    };

    template <typename Engine>
    class MaxPilePlayer : public Player<Engine>
    {
        using Player<Engine>::gameState;
    public:
        MaxPilePlayer(GameState<Engine>& newGameState) : Player<Engine>(newGameState) {}

        virtual void MakeMove() override
        {
            vector<BoundedPile> piles = gameState.GetPiles();
            if (gameState.IsCardDrawn())
            {
                vector<unsigned int> availablePiles;
                for (unsigned int currentPile = 0; currentPile < gameState.GetNumberOfPiles(); ++currentPile)
                {
                    if ((gameState.GetNumberOfCardsInPile(currentPile) != gameState.GetMaximumCardsPerPile(currentPile)) &&
                        !gameState.IsPileTaken(currentPile))
                    {
                        availablePiles.push_back(currentPile);
                    }
                }
                gameState.PlaceCardOnPile(availablePiles.back());
            }
            else
            {
                uniform_int_distribution<unsigned int> pileDistribution(0, gameState.GetNumberOfPiles() - 1);
                if (all_of(piles.begin(), piles.end(), [] (BoundedPile pile) { return pile.isTaken || (pile.GetNumberOfCards() == pile.GetCapacity()); }))
                {
                    unsigned int pileToTake = 0;
                    while (gameState.IsPileTaken(pileToTake))
                    {
                        ++pileToTake;
                    }
                    gameState.TakePile(pileToTake);
                }
                else if (all_of(piles.begin(), piles.end(), [] (BoundedPile pile) { return pile.GetNumberOfCards() == 0; }))
                {
                    gameState.DrawCard();
                }
                else if (any_of(piles.begin(), piles.end(), [] (BoundedPile pile) { return pile.GetNumberOfCards() == pile.GetCapacity(); }))
                {
                    unsigned int pilesBefore = gameState.GetNumberOfPiles();
                    unsigned int currentPile = 0;
                    for (; currentPile < gameState.GetNumberOfPiles(); ++currentPile)
                    {
                        if ((gameState.GetNumberOfCardsInPile(currentPile) == gameState.GetMaximumCardsPerPile(currentPile)) && 
                            !gameState.IsPileTaken(currentPile))
                        {
                            gameState.TakePile(currentPile);
                            break;
                        }
                    }
                }
                else
                {
                    gameState.DrawCard();
                }
            }
        }
    private:
        Engine engine;
    };

    template <typename Engine>
    class RandomPlayer : public Player<Engine>
    {
        using Player<Engine>::gameState;
    public:
        RandomPlayer(GameState<Engine>& newGameState, Engine& newEngine) : Player<Engine>(newGameState)
        {
            engine = newEngine;
        }

        virtual void MakeMove() override
        {
            vector<BoundedPile> piles = gameState.GetPiles();
            if (gameState.IsCardDrawn())
            {
                vector<unsigned int> availablePiles;
                for (unsigned int currentPile = 0; currentPile < gameState.GetNumberOfPiles(); ++currentPile)
                {
                    if ((gameState.GetNumberOfCardsInPile(currentPile) != gameState.GetMaximumCardsPerPile(currentPile)) &&
                        !gameState.IsPileTaken(currentPile))
                    {
                        availablePiles.push_back(currentPile);
                    }
                }
                shuffle(availablePiles.begin(), availablePiles.end(), engine);
                gameState.PlaceCardOnPile(availablePiles.back());
            }
            else
            {
                uniform_int_distribution<unsigned int> pileDistribution(0, gameState.GetNumberOfPiles() - 1);
                if (all_of(piles.begin(), piles.end(), [] (BoundedPile pile) { return pile.isTaken || (pile.GetNumberOfCards() == pile.GetCapacity()); }))
                {
                    unsigned int pileToTake = pileDistribution(engine);
                    while (gameState.IsPileTaken(pileToTake))
                    {
                        pileToTake = (pileToTake + 1) % gameState.GetNumberOfPiles();
                    }
                    gameState.TakePile(pileToTake);
                }
                else if (all_of(piles.begin(), piles.end(), [] (BoundedPile pile) { return pile.GetNumberOfCards() == 0; }))
                {
                    gameState.DrawCard();
                }
                else if (bernoulliDistribution(engine))
                {
                    gameState.DrawCard();
                }
                else
                {
                    vector<unsigned int> availablePiles;
                    for (unsigned int currentPile = 0; currentPile < gameState.GetNumberOfPiles(); ++currentPile)
                    {
                        if ((gameState.GetNumberOfCardsInPile(currentPile) > 0) && !gameState.IsPileTaken(currentPile))
                        {
                            availablePiles.push_back(currentPile);
                        }
                    }
                    shuffle(availablePiles.begin(), availablePiles.end(), engine);
                    gameState.TakePile(availablePiles.back());
                }
            }
        }
    private:
        Engine engine;
        bernoulli_distribution bernoulliDistribution;
    };

    template <typename Engine>
    class ImmediatePlayer : public Player<Engine>
    {
        using Player<Engine>::gameState;
    public:
        ImmediatePlayer(GameState<Engine>& newGameState) : Player<Engine>(newGameState) {}

        virtual void MakeMove() override
        {
            vector<BoundedPile> piles = gameState.GetPiles();
            if (gameState.IsCardDrawn())
            {
                vector<unsigned int> availablePiles;
                for (unsigned int currentPile = 0; currentPile < gameState.GetNumberOfPiles(); ++currentPile)
                {
                    if ((gameState.GetNumberOfCardsInPile(currentPile) != gameState.GetMaximumCardsPerPile(currentPile)) && !gameState.IsPileTaken(currentPile))
                    {
                        availablePiles.push_back(currentPile);
                    }
                }
                gameState.PlaceCardOnPile(availablePiles.back());
            }
            else
            {
                uniform_int_distribution<unsigned int> pileDistribution(0, gameState.GetNumberOfPiles() - 1);
                if (all_of(piles.begin(), piles.end(), [] (BoundedPile pile) { return pile.isTaken || (pile.GetNumberOfCards() == pile.GetCapacity()); }))
                {
                    unsigned int pileToTake = 0;
                    while (gameState.IsPileTaken(pileToTake))
                    {
                        ++pileToTake;
                    }
                    gameState.TakePile(pileToTake);
                }
                else if (all_of(piles.begin(), piles.end(), [] (BoundedPile pile) { return pile.GetNumberOfCards() == 0; }))
                {
                    gameState.DrawCard();
                }
                else 
                {
                    vector<unsigned int> availablePiles;
                    for (unsigned int currentPile = 0; currentPile < gameState.GetNumberOfPiles(); ++currentPile)
                    {
                        if ((gameState.GetNumberOfCardsInPile(currentPile) > 0) && !gameState.IsPileTaken(currentPile))
                        {
                            availablePiles.push_back(currentPile);
                        }
                    }
                    gameState.TakePile(availablePiles.back());
                }
            }
        }
    };
}
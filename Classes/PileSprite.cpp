#include "cocos2d.h"
#include "engine\Card.h"
#include "engine\Pile.h"
#include "CardSprite.h"
#include "PileSprite.h"

USING_NS_CC;
using bulbul::BoundedPile;
using bulbul::Card;

void PileSprite::setOpacity(GLubyte opacity)
{
    for (auto child : getChildren())
    {
        child->setOpacity(opacity);
    }
}

const Size& PileSprite::getContentSize() const
{
    return contentSize;
}

void PileSprite::update(float delta)
{
    unsigned int currentCard = 0;
    for (Card card : pile->GetCardTypes())
    {
        for (unsigned int currentCardInstance = 0; currentCardInstance < pile->GetCardCount(card); currentCardInstance++)
        {
            cardSprites[currentCard]->ChangeCard(card);
            ++currentCard;
        }
    }
    for (; currentCard < pile->GetCapacity(); ++currentCard)
    {
        cardSprites[currentCard]->ChangeCard(Card::PAST_ALL_CARDS);
    }
}

PileSprite* PileSprite::CreatePileSprite(const BoundedPile* pile)
{
    auto pileSprite = PileSprite::create();
    pileSprite->pile = pile;

    auto testTubeSprite = Sprite::create();
    testTubeSprite->setTexture("TestTube.png");
    auto testTubeSpriteSize = testTubeSprite->getContentSize();
    testTubeSprite->setScale(160.0f / testTubeSpriteSize.width);
    testTubeSprite->setPosition(160.0f / 2, 149.0f / 2);
    pileSprite->addChild(testTubeSprite, 0);
    pileSprite->contentSize.setSize(testTubeSpriteSize.width, testTubeSpriteSize.height);

    int currentZ = 1;
    float currentXY = 25.0f;
    unsigned int cardsAdded = 0;
    for (Card card : pile->GetCardTypes())
    {
        for (unsigned int currentCard = 0; currentCard < pile->GetCardCount(card); ++currentCard)
        {
            auto cardSprite = CardSprite::CreateCardSprite(card);
            auto cardSpriteSize = cardSprite->getContentSize();
            cardSprite->setScale(50.0f / cardSpriteSize.height);
            cardSprite->setPosition(currentXY, currentXY);
            pileSprite->addChild(cardSprite, currentZ);
            ++currentZ;
            currentXY += 25.0f;
            ++cardsAdded;
            pileSprite->cardSprites.push_back(cardSprite);
        }
    }
    for (; cardsAdded < pile->GetCapacity(); ++cardsAdded)
    {
        auto cardSprite = CardSprite::CreateCardSprite(Card::PAST_ALL_CARDS);
        auto cardSpriteSize = cardSprite->getContentSize();
        cardSprite->setScale(50.0f / cardSpriteSize.height);
        cardSprite->setPosition(currentXY, currentXY);
        pileSprite->addChild(cardSprite, currentZ);
        ++currentZ;
        currentXY += 25.0f;
        pileSprite->cardSprites.push_back(cardSprite);
    }

    return pileSprite;
}
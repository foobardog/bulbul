#include <algorithm>
#include <string>
#include <utility>
#include <vector>
#include "cocos2d.h"
#include "ui\CocosGUI.h"
#include "engine\Pile.h"
#include "CardSprite.h"
#include "InfoScreenSprite.h"


USING_NS_CC;
using namespace ui;
using std::make_pair;
using std::pair;
using std::sort;
using std::to_string;
using std::vector;
using bulbul::Pile;

const Size& InfoScreenSprite::getContentSize() const
{
    return contentSize;
}

void InfoScreenSprite::ShowPileInformation(const Pile* pile, ScoringMap scoringMap)
{
    removeAllChildren();
    CreateChildSprites();

    auto titleLabel = Text::create("Genetic Makeup", "fonts/retro_party.ttf", 30);
    titleLabel->setColor(Color3B(32, 32, 32));
    titleLabel->setPosition(Vec2(425.0f, 360.0f));
    this->addChild(titleLabel);

    string scoreString = "Mendel Score: " + to_string(pile->ScorePile(scoringMap));
    auto scoreLabel = Text::create(scoreString, "fonts/retro_party.ttf", 30);
    scoreLabel->setColor(Color3B(32, 32, 32));
    scoreLabel->setPosition(Vec2(425.0f, 35.0f));
    this->addChild(scoreLabel);

    vector<pair<Card, unsigned int>> cardCountVector;
    for (Card cardType : pile->GetCardTypes())
    {
        cardCountVector.push_back(make_pair(cardType, pile->GetCardCount(cardType)));
    }
    sort(cardCountVector.begin(), 
        cardCountVector.end(), 
        [] (pair<Card, unsigned int>& a, pair<Card, unsigned int>& b) { return a.second > b.second; });

    float currentColumn = 0;
    float currentX = 100.0f;
    float currentRow = 0;
    float currentY = 300.0f;
    for (pair<Card, unsigned int> cardCountPair : cardCountVector)
    {
        Card cardType = cardCountPair.first;
        auto cardSprite = CardSprite::CreateCardSprite(cardType);
        auto cardSpriteSize = cardSprite->getContentSize();
        cardSprite->setScale(75.0f / cardSpriteSize.height);
        cardSprite->setPosition(currentX, currentY);
        this->addChild(cardSprite);

        unsigned int cardCount = cardCountPair.second;
        auto cardLabel = Text::create(to_string(cardCount), "fonts/retro_party.ttf", 30);
        cardLabel->setColor(Color3B(32, 32, 32));
        cardLabel->setPosition(Vec2(currentX + 75.0f, currentY));
        this->addChild(cardLabel);

        ++currentColumn;
        if (currentColumn >= 3)
        {
            currentX = 100.0f;
            currentY -= 100.0f;
            currentColumn = 0;
            ++currentRow;
        }
        else
        {
            currentX += 200.0f;
        }
    }
}

InfoScreenSprite* InfoScreenSprite::CreateInfoScreenSprite()
{
    auto infoScreenSprite = InfoScreenSprite::create();
    infoScreenSprite->CreateChildSprites();
    return infoScreenSprite;
}

void InfoScreenSprite::CreateChildSprites()
{
    auto backgroundSprite = Sprite::create("InfoBackground.png");
    auto backgroundSpriteSize = backgroundSprite->getContentSize();
    backgroundSprite->setScale(400.0f / backgroundSpriteSize.height);
    backgroundSprite->setPosition(400.0f, 200.0f);
    this->addChild(backgroundSprite, 0);
}
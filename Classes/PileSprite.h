#pragma once

#include <vector>
#include "cocos2d.h"
#include "engine\Pile.h"
#include "CardSprite.h"

USING_NS_CC;
using std::vector;
using bulbul::BoundedPile;

class PileSprite : public cocos2d::Sprite
{
public:
    void setOpacity(GLubyte opacity) override;
    const Size& getContentSize() const override;
    void update(float delta) override;
    CREATE_FUNC(PileSprite);

    static PileSprite* CreatePileSprite(const BoundedPile* pile);
private:
    const BoundedPile* pile;
    Size contentSize;
    vector<CardSprite*> cardSprites;
};
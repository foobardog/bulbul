#include "cocos2d.h"
#include "engine\Card.h"
#include "engine\Pile.h"
#include "CardSprite.h"
#include "PlayerCardsSprite.h"

USING_NS_CC;
using bulbul::Pile;

void PlayerCardsSprite::setOpacity(GLubyte opacity)
{
    flaskSprite->setOpacity(opacity);
}

const Size& PlayerCardsSprite::getContentSize() const
{
    return contentSize;
}

void PlayerCardsSprite::update(float delta)
{
    this->removeAllChildren();
    this->CreateChildSprites();
}

const Pile* PlayerCardsSprite::GetPile() const
{
    return pile;
}

PlayerCardsSprite* PlayerCardsSprite::CreatePlayerCardsSprite(const Pile* pile)
{
    auto playerCardsSprite = PlayerCardsSprite::create();
    playerCardsSprite->pile = pile;
    playerCardsSprite->CreateChildSprites();
    playerCardsSprite->contentSize.setSize(240.0f, 240.0f);
    return playerCardsSprite;
}

void PlayerCardsSprite::CreateChildSprites()
{
    auto flaskSprite = Sprite::create();
    flaskSprite->setTexture("Flask.png");
    auto flaskSpriteSize = flaskSprite->getContentSize();
    flaskSprite->setScale(209.0f / flaskSpriteSize.width);
    flaskSprite->setPosition(209.0f / 2, 240.0f / 2);
    this->addChild(flaskSprite, 0);
    this->flaskSprite = flaskSprite;

    int currentColumn = 0;
    int currentRow = 0;

    for (Card card : pile->GetCardTypes())
    {
        float currentX = 20.0f + (80.0f * currentColumn);
        float currentY = 20.0f + (80.0f * currentRow);
        int currentZ = 1;

        for (unsigned int currentCard = 0; currentCard < pile->GetCardCount(card); ++currentCard)
        {
            auto cardSprite = CardSprite::CreateCardSprite(card);
            auto cardSpriteSize = cardSprite->getContentSize();
            cardSprite->setScale(40.0f / cardSpriteSize.height);
            cardSprite->setPosition(currentX, currentY);
            this->addChild(cardSprite, currentZ);
            currentX += 5.0f;
            currentY += 5.0f;
            ++currentZ;
        }

        if (++currentColumn == 3)
        {
            currentColumn = 0;
            ++currentRow;
        }
    }
}
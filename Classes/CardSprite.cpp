#include "cocos2d.h"
#include "engine\Card.h"
#include "CardSprite.h"

USING_NS_CC;
using bulbul::Card;

CardSprite* CardSprite::CreateCardSprite(Card card)
{
    auto cardSprite = CardSprite::create();
    cardSprite->card = card;
    switch (card)
    {
    case Card::JOKER:
        cardSprite->initWithFile("Joker.png");
        break;
    case Card::PLUS_TWO:
        cardSprite->initWithFile("PlusTwo.png");
        break;
    case Card::RAT:
        cardSprite->initWithFile("Rat.png");
        break;
    case Card::RABBIT:
        cardSprite->initWithFile("Rabbit.png");
        break;
    case Card::SNAKE:
        cardSprite->initWithFile("Snake.png");
        break;
    case Card::SHEEP:
        cardSprite->initWithFile("Sheep.png");
        break;
    case Card::MONKEY:
        cardSprite->initWithFile("Monkey.png");
        break;
    case Card::ROOSTER:
        cardSprite->initWithFile("Rooster.png");
        break;
    case Card::DOG:
        cardSprite->initWithFile("Dog.png");
        break;
    default:
        cardSprite->initWithFile("DNA.png");
        break;
    }
    return cardSprite;
}

void CardSprite::ChangeCard(Card card)
{
    this->card = card;
    switch (card)
    {
    case Card::JOKER:
        setTexture("Joker.png");
        break;
    case Card::PLUS_TWO:
        setTexture("PlusTwo.png");
        break;
    case Card::RAT:
        setTexture("Rat.png");
        break;
    case Card::RABBIT:
        setTexture("Rabbit.png");
        break;
    case Card::SNAKE:
        setTexture("Snake.png");
        break;
    case Card::SHEEP:
        setTexture("Sheep.png");
        break;
    case Card::MONKEY:
        setTexture("Monkey.png");
        break;
    case Card::ROOSTER:
        setTexture("Rooster.png");
        break;
    case Card::DOG:
        setTexture("Dog.png");
        break;
    default:
        setTexture("DNA.png");
        break;
    }
}
#pragma once

#include "cocos2d.h"
#include "engine\Card.h"

USING_NS_CC;
using bulbul::Card;

class CardSprite : public Sprite
{
public:
    CREATE_FUNC(CardSprite);
   
    static CardSprite* CreateCardSprite(Card card);
    void ChangeCard(Card card);
private:
    Card card;
};
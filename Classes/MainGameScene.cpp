#include <ctime>
#include <random>
#include <utility>
#include "cocos2d.h"
#include "ui\CocosGUI.h"
#include "engine\GameState.h"
#include "engine\Pile.h"
#include "engine\Player.h"
#include "CardSprite.h"
#include "InfoScreenSprite.h"
#include "PileSprite.h"
#include "PlayerCardsSprite.h"
#include "MainGameScene.h"

USING_NS_CC;
using std::default_random_engine;
using std::pair;
using bulbul::BoundedPile;
using bulbul::Card;
using bulbul::GameState;
using bulbul::ImmediatePlayer;
using bulbul::MaxPilePlayer;
using bulbul::Pile;
using bulbul::Player;
using bulbul::RandomPlayer;

Scene* MainGame::createScene()
{
    auto scene = Scene::create();
    auto layer  = MainGame::create();
    scene->addChild(layer);
    return scene;
}

bool MainGame::init()
{
    if (!Layer::init())
    {
        return false;
    }

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto backgroundSprite = Sprite::create("Background.png");
    auto backgroundSpriteSize = backgroundSprite->getContentSize();
    backgroundSprite->setScale(960.0f / backgroundSpriteSize.width);
    backgroundSprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
    this->addChild(backgroundSprite, 0);

    engine.seed(time(nullptr));
    shared_ptr<GameState<default_random_engine>> newGameState(new GameState<default_random_engine>(5, engine));
    gameState = newGameState;

    shared_ptr<Player<default_random_engine>> maxPilePlayer(new MaxPilePlayer<default_random_engine>(*gameState));
    playerMap.insert(pair<unsigned int, shared_ptr<Player<default_random_engine>>>(1, maxPilePlayer));
    playerMap.insert(pair<unsigned int, shared_ptr<Player<default_random_engine>>>(4, maxPilePlayer));

    shared_ptr<Player<default_random_engine>> immediatePlayer(new ImmediatePlayer<default_random_engine>(*gameState));
    playerMap.insert(pair<unsigned int, shared_ptr<Player<default_random_engine>>>(2, immediatePlayer));

    shared_ptr<Player<default_random_engine>> randomPlayer(new RandomPlayer<default_random_engine>(*gameState, engine));
    playerMap.insert(pair<unsigned int, shared_ptr<Player<default_random_engine>>>(3, randomPlayer));

    // Deck
    auto deckSprite = CardSprite::CreateCardSprite(gameState->NO_CARD_DRAWN);
    auto deckSpriteSize = deckSprite->getContentSize();
    deckSprite->setScale(150.0f / deckSpriteSize.height);
    deckSprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height + origin.y - 75.0f));
    this->addChild(deckSprite, 1);

    // Drawn Card
    auto drawnCardSprite = CardSprite::CreateCardSprite(gameState->NO_CARD_DRAWN);
    auto drawnCardSpriteSize = drawnCardSprite->getContentSize();
    drawnCardSprite->setScale(150.0f / drawnCardSpriteSize.height);
    drawnCardSprite->setPosition(Vec2(visibleSize.width / 2 + origin.x - 15.0f, visibleSize.height + origin.y - 90.0f));
    drawnCardSprite->setVisible(false);
    this->addChild(drawnCardSprite, 3);

    // Current Player Marker
    auto currentPlayerSprite = Sprite::create("CurrentPlayerMarker.png");
    auto currentPlayerSpriteSize = currentPlayerSprite->getContentSize();
    currentPlayerSprite->setScale(240.0f / currentPlayerSpriteSize.height);
    currentPlayerSprite->setPosition(origin.x + 120.0f, visibleSize.height + origin.y - 120.0f);
    this->addChild(currentPlayerSprite, 1);

    // Final Round Marker
    auto finalRoundSprite = Sprite::create("FinalRoundMarker.png");
    auto finalRoundSpriteSize = finalRoundSprite->getContentSize();
    finalRoundSprite->setScale(150.0f / finalRoundSpriteSize.height);
    finalRoundSprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height + origin.y - 75.0f));
    finalRoundSprite->setVisible(false);
    this->addChild(finalRoundSprite, 1);

    // Info screen
    auto infoScreenSprite = InfoScreenSprite::CreateInfoScreenSprite();
    infoScreenSprite->setPosition(origin.x + 80.0f, origin.y + 120.0f);
    infoScreenSprite->setVisible(false);
    addChild(infoScreenSprite, 20);

    auto infoScreenListener = EventListenerTouchOneByOne::create();
    infoScreenListener->setSwallowTouches(true);

    infoScreenListener->onTouchBegan = [=] (Touch* touch, Event* event) {
        auto target = static_cast<Sprite*>(event->getCurrentTarget());

        if (target->isVisible())
        {
            target->setVisible(false);
            return true;
        }
        return false;
    };

    _eventDispatcher->addEventListenerWithSceneGraphPriority(infoScreenListener, infoScreenSprite);

    // Player Cards
    const vector<Pile>& playerCardPiles = gameState->GetAllPlayerCards();
    unsigned int currentPlayerCardsPile = 0;
    for (const Pile& pile : playerCardPiles)
    {
        auto playerCardsSprite = PlayerCardsSprite::CreatePlayerCardsSprite(&pile);
        playerCardsSprites.push_back(playerCardsSprite);
        switch(currentPlayerCardsPile)
        {
        case 0:
            playerCardsSprite->setPosition(origin.x, visibleSize.height + origin.y - 240.0f);
            break;
        case 1:
            playerCardsSprite->setPosition(origin.x, origin.y);
            break;
        case 2:
            playerCardsSprite->setPosition(visibleSize.width / 2 + origin.x - 120.0f, origin.y);
            break;
        case 3:
            playerCardsSprite->setPosition(visibleSize.width + origin.x - 240.0f, origin.y);
            break;
        case 4:
            playerCardsSprite->setPosition(visibleSize.width + origin.x - 240.0f, visibleSize.height + origin.y - 240.0f);
            break;
        }
        this->addChild(playerCardsSprite, 2);
        ++currentPlayerCardsPile;

        auto playerCardsListener = EventListenerTouchOneByOne::create();
        playerCardsListener->setSwallowTouches(true);

        playerCardsListener->onTouchBegan = [=] (Touch* touch, Event* event) {
            auto target = static_cast<Sprite*>(event->getCurrentTarget());

            Vec2 locationInNode = target->convertToNodeSpace(touch->getLocation());
            Size spriteSize = target->getContentSize();
            Rect rect = Rect(0, 0, 240.0f, 240.0f);

            if (rect.containsPoint(locationInNode))
            {
                return true;
            }
            return false;
        };

        playerCardsListener->onTouchEnded = [=] (Touch* touch, Event* event) {
            auto target = static_cast<PlayerCardsSprite*>(event->getCurrentTarget());
            infoScreenSprite->setVisible(true);
            infoScreenSprite->ShowPileInformation(target->GetPile(), Pile::additiveScoringMap);
        };

        _eventDispatcher->addEventListenerWithSceneGraphPriority(playerCardsListener, playerCardsSprite);
    }


    // Piles
    const vector<BoundedPile>& piles = gameState->GetPiles();
    unsigned int currentPile = 0;
    for (const BoundedPile& pile : piles)
    {
        auto pileSprite = PileSprite::CreatePileSprite(&pile);
        pileSprites.push_back(pileSprite);
        switch(currentPile)
        {
        case 0:
            pileSprite->setPosition(visibleSize.width / 2 + origin.x - 240.0f, visibleSize.height + origin.y - 160.0f);
            break;
        case 1:
            pileSprite->setPosition(visibleSize.width / 2 + origin.x - 240.0f, visibleSize.height + origin.y - 320.0f);
            break;
        case 2:
            pileSprite->setPosition(visibleSize.width / 2 + origin.x - 80.0f, visibleSize.height + origin.y - 320.0f);
            break;
        case 3:
            pileSprite->setPosition(visibleSize.width / 2 + origin.x + 80.0f, visibleSize.height + origin.y - 320.0f);
            break;
        case 4:
            pileSprite->setPosition(visibleSize.width / 2 + origin.x + 80.0f, visibleSize.height + origin.y - 160.0f);
            break;
        }
        this->addChild(pileSprite, 1);

        auto pileListener = EventListenerTouchOneByOne::create();
        pileListener->setSwallowTouches(true);

        pileListener->onTouchBegan = [=] (Touch* touch, Event* event) {
            auto target = static_cast<Sprite*>(event->getCurrentTarget());

            Vec2 locationInNode = target->convertToNodeSpace(touch->getLocation());
            Size spriteSize = target->getContentSize();
            Rect rect = Rect(0, 0, 160.0f, 149.0f);

            if (rect.containsPoint(locationInNode) && !gameState->IsPileTaken(currentPile))
            {
                target->setOpacity(180);
                return true;
            }
            return false;
        };

        pileListener->onTouchEnded = [=] (Touch* touch, Event* event) {
            auto target = static_cast<Sprite*>(event->getCurrentTarget());
            target->setOpacity(255);
            if (gameState->IsCardDrawn() && !gameState->GetPile(currentPile).IsFull())
            {
                gameState->PlaceCardOnPile(currentPile);
                drawnCardSprite->setVisible(false);
                target->update(0.0f);
            }
            else if (!gameState->IsCardDrawn() && gameState->GetPile(currentPile).GetNumberOfCards() > 0)
            {
                gameState->TakePile(currentPile);
                unsigned int currentPileIndex = 0;
                for (auto currentPileSprite : pileSprites)
                {
                    currentPileSprite->setVisible(!gameState->IsPileTaken(currentPileIndex));
                    currentPileSprite->update(0.0f);
                    ++currentPileIndex;
                }
                for (auto child : this->getChildren())
                {
                    child->update(0.0f);
                }
            }

            while (playerMap.find(gameState->GetCurrentPlayer()) != playerMap.end() && !gameState->IsGameOver())
            {
                playerMap.at(gameState->GetCurrentPlayer())->MakeMove();
                for (auto child : this->getChildren())
                {
                    child->update(0.0f);
                }
                unsigned int currentPileIndex = 0;
                for (auto currentPileSprite : pileSprites)
                {
                    currentPileSprite->setVisible(!gameState->IsPileTaken(currentPileIndex));
                    ++currentPileIndex;
                }
            }

            for (unsigned int playerIndex : gameState->GetPlayersThatHaveTaken())
            {
                (*(playerCardsSprites.begin() + playerIndex))->setOpacity(90);
            }

            if (gameState->IsGameOver())
            {
                currentPlayerSprite->setVisible(false);
            }
            else
            {
                switch (gameState->GetCurrentPlayer())
                {
                case 0:
                    currentPlayerSprite->setPosition(origin.x + 120.0f, visibleSize.height + origin.y - 120.0f);
                    break;
                case 1:
                    currentPlayerSprite->setPosition(origin.x + 120.0f, origin.y + 120.0f);
                    break;
                case 2:
                    currentPlayerSprite->setPosition(visibleSize.width / 2 + origin.x, origin.y + 120.0f);
                    break;
                case 3:
                    currentPlayerSprite->setPosition(visibleSize.width + origin.x - 120.0f, origin.y + 120.0f);
                    break;
                case 4:
                    currentPlayerSprite->setPosition(visibleSize.width + origin.x - 120.0f, visibleSize.height + origin.y - 120.0f);
                    break;
                default:
                    currentPlayerSprite->setPosition(origin.x, origin.y);
                    break;
                }
            }
        };

        _eventDispatcher->addEventListenerWithSceneGraphPriority(pileListener, pileSprite);

        ++currentPile;
    }

    auto deckListener = EventListenerTouchOneByOne::create();
    deckListener->setSwallowTouches(true);

    deckListener->onTouchBegan = [=] (Touch* touch, Event* event) {
        auto target = static_cast<Sprite*>(event->getCurrentTarget());

        Vec2 locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size spriteSize = target->getContentSize();
        Rect rect = Rect(0, 0, 150.0f, 150.0f);

        if (rect.containsPoint(locationInNode) && !gameState->IsCardDrawn() && !gameState->AllPilesFull() && !gameState->IsGameOver())
        {
            target->setOpacity(180);
            return true;
        }
        return false;
    };

    deckListener->onTouchEnded = [=] (Touch* touch, Event* event) {
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        target->setOpacity(255);
        gameState->DrawCard();
        drawnCardSprite->ChangeCard(gameState->GetDrawnCard());
        drawnCardSprite->setVisible(true);
        finalRoundSprite->setVisible(gameState->IsFinalRound());
    };

    _eventDispatcher->addEventListenerWithSceneGraphPriority(deckListener, deckSprite);


    return true;
}

#pragma once

#include "cocos2d.h"
#include "engine\Pile.h"

USING_NS_CC;
using bulbul::Pile;
using bulbul::ScoringMap;

class InfoScreenSprite : public Sprite
{
public:
    const Size& getContentSize() const override;
    CREATE_FUNC(InfoScreenSprite);

    static InfoScreenSprite* CreateInfoScreenSprite();
    void ShowPileInformation(const Pile* pile, ScoringMap scoringMap);
private:
    Size contentSize;
    void CreateChildSprites();
};
#pragma once

#include <map>
#include <memory>
#include <random>
#include <vector>
#include "cocos2d.h"
#include "engine\GameState.h"
#include "engine\Player.h"
#include "InfoScreenSprite.h"
#include "PileSprite.h"
#include "PlayerCardsSprite.h"

using std::default_random_engine;
using std::map;
using std::shared_ptr;
using std::vector;
using bulbul::GameState;
using bulbul::Player;

class MainGame : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init() override;
    //virtual void update(float delta) override;

    CREATE_FUNC(MainGame);
private:
    default_random_engine engine;
    shared_ptr<GameState<default_random_engine>> gameState;
    vector<PileSprite*> pileSprites;
    vector<PlayerCardsSprite*> playerCardsSprites;
    InfoScreenSprite* infoScreenSprite;
    map<unsigned int, shared_ptr<Player<default_random_engine>>> playerMap;
};
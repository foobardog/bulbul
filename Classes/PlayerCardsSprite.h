#pragma once

#include "cocos2d.h"
#include "engine\Pile.h"

USING_NS_CC;
using bulbul::Pile;

class PlayerCardsSprite : public Sprite
{
public:
    void setOpacity(GLubyte opacity) override;
    const Size& getContentSize() const override;
    void update(float delta) override;
    CREATE_FUNC(PlayerCardsSprite);

    const Pile* GetPile() const;
    static PlayerCardsSprite* CreatePlayerCardsSprite(const Pile* pile);
private:
    const Pile* pile;
    Size contentSize;
    Sprite* flaskSprite;

    void CreateChildSprites();
};